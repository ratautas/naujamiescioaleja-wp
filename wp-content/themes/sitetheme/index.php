<?php

get_header();


?>
<div>  
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class("page-sitenews-item"); ?>>
                    <div class="page-sitenews-item-l">                          
                        <div class="page-sitenews-item-l-img">   
                        <?php 
                        if (has_post_thumbnail()) { 
                                 $urlicon = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'product-list' );
                        ?>
                            <?php 
                            if ( is_singular() ) :
                            ?>
                            <img src="<?php echo $urlicon[0]; ?>">
                            <?php else : ?>
                            <a href="<?php the_permalink(); ?>" aria-hidden="true"><img src="<?php echo $urlicon[0]; ?>"></a>
                            <?php endif; 
                            ?> 
                        <?php                          
                        }
                        ?>
                        </div>
                    </div>    
                    <div class="page-sitenews-item-r">
                        <?php if (is_single()) : ?>
                            <h1><?php the_title(); ?></h1>
                        <?php else : ?>
                            <h1>
                                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                            </h1> 
                        <?php endif; ?>
                        <?php if (is_single()) : ?>
                              <div class="page-sitenews-item-r-cont">
                            <?php the_content(); ?>
                            </div>
                        <?php else : ?>
                              <div class="page-sitenews-item-r-cont">
                            <?php the_excerpt(); ?>
                            </div>
                        <?php endif; ?>  
                    </div>
                </div>
        <?php endwhile; ?>                
        <div style="clear: both;"></div>    
        <div class="temp-sitenews-pagin">
            <div class="page-link">
                 <?php 
                 site_paging_nav();
                 ?>
            </div>
        </div>  
        <?php else : ?>
        <h1><?php echo ''; ?></h1>
        <?php endif; ?>

</div>
<?php
get_footer(); 
?>