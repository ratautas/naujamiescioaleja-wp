/*jslint plusplus: true*/
/*jslint nomen: true*/
/*global $, jQuery, alert, google, console, outdatedBrowser, IScroll, FastClick, videojs*/

var markers, featurescroll, deskgallery, docid, pos, bodyscroll,
  disabler, area, rooms, demo, map, loadScript, latitude,
  longitude, addMarker, winw, winh, mobgallery, slidecount,
  slidecurr, rescale, slider, elementIndex, places, addPlaces,
  marker, root_url, bublepos, slidersize, slidenum, floorid, hoverclass,
  houseid, bubblepos, InfoBubble, mapZoom, scale, setLayouts, _, pagenum,
  setMarkers, shape, mobslidenum, mobslidecount, buboffsetY, buboffsetX, sliderName,
  space, filterval, conth, contw, calcw, calch, floor, ie,
  ms_ie, ua, old_ie, new_ie, tablet, bublong, bublat, scaleSVG, doSomething,
  detectIPadOrientation, orientation, scrolls, scroll, currpage, flatscroll,
  mobease, floorsel, scrollwheel, free, sold, reserved, hover, state,
  touchstart, touchtime, noScroll,
  is_touch = (typeof (window.ontouchstart) !== "undefined") || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0),
  scrolling = false;

$(document).ready(function () {
  "use strict";
  window.viewportUnitsBuggyfill.init({
    force: true
  });
  ie();
  FastClick.attach(document.body);
  if ($('#map-canvas').length) {
    loadScript();
  }
  winw = $(window).width();
  winh = $(window).height();
  tablet();
  scrolls();
  if ($('.desk-features').length) {
    featurescroll = new IScroll('.desk-features', {
      scrollX: false,
      scrollY: true,
      mouseWheel: true,
      scrollbars: true,
      interactiveScrollbars: true
    });
  }
  if ($('.floor-plan').attr('id') !== undefined) {
    floorsel = $('.floor-plan').attr('id').replace('floor-', '#floorsel-');
    $(floorsel).addClass('active');
    setMarkers();
  }
  //  if (winw < 769) {
  //    bodyscroll.disable();
  //  }
  if (winw >= 1023) {
    scaleSVG();
  }
  if (is_touch) {
    $('body').addClass('touch');
  }


  //   add active class for floor selection
  //  if ($('.floor-page').length) {
  //  }

});


//var lazyLayout = _.debounce(setLayouts, 300);
//$(window).resize(lazyLayout);
//$(window).resize(function () {
//  "use strict";
//  setLayouts();
//});

$(window).resize(function () {
  'use strict';
  winw = $(window).width();
  winh = $(window).height();
  tablet();
  scaleSVG();
  scrolls();
});

//$(window).bind('orientationchange', function () {
//  'use strict';
//  winw = $(window).width();
//  winh = $(window).height();
//  tablet();
//  scaleSVG();
//  scrolls();
//});

function tablet() {
  'use strict';
  if (winw < 1023 && winw >= 768 && is_touch) {
    //    $('.wrapper').hide();
    $('.tablet-err').show(); //fadeIn(300);
  } else {
    $('.tablet-err').hide(); //fadeOut(300);
    //    $('.tablet-err').hide();
    //    $('.wrapper').fadeIn(300);
  }
  //  if ($('.desk-gallery').is(':visible') && typeof deskgallery !== 'undefined') {
  //    deskgallery.goToPage(1, 0, 0);
  //  }
}

$(document).on('touchstart', '.desk-features', function () {
  'use strict';
  bodyscroll.disable();
});

$(document).on('touchend', '.desk-features', function () {
  'use strict';
  bodyscroll.enable();
});


////////////////////////////////////////// GOOGLE MAP JSLinted

function loadScript() {
  "use strict";
  jQuery.getScript("http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobubble/src/infobubble.js", function (data, status, jqxhr) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&callback=initializeMap';
    document.body.appendChild(script);
  });
}

function initializeMap() {
  "use strict";
  var latlng = new google.maps.LatLng(latitude, longitude),
    mapZoom = 16;
  if (winw < 769) {
    mapZoom = 15;
  }
  var myOptions = {
    zoom: mapZoom,
    minZoom: mapZoom - 3,
    maxZoom: mapZoom + 3,
    navigationControl: false,
    scaleControl: true,
    draggable: true,
    center: latlng,
    scrollwheel: false,
    streetViewControl: false,
    panControl: false,
    mapTypeControl: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
  if (typeof places !== 'undefined') {
    addPlaces();
  } else {
    addMarker();
  }
  return true;
}

function addMarker() {
  "use strict";
  var location = new google.maps.LatLng(latitude, longitude),
    marker = new google.maps.Marker({
      position: location,
      map: map
    }),
    infoWindow = new google.maps.InfoWindow();
  google.maps.event.addListener(marker, 'click', function () {
    map.setCenter(location);
    infoWindow.open(map, marker);
    map.setZoom(mapZoom);
  });
  map.setZoom(mapZoom);
}

function addPlaces() {
  "use strict";
  $.each(places, function (k, v) {
    scale = Math.pow(2, (21 - map.getZoom()));
    var pa = root_url + '/img/callout-' + v[0] + '.svg',
      pahover = root_url + '/img/callout-' + v[0] + '-hover.svg',
      location = new google.maps.LatLng(v[1], v[2]),
      marker = new google.maps.Marker({
        position: location,
        icon: pa,
        map: map
      }),

      newlong = marker.getPosition().lng() + (0.000048 * Math.pow(2, (21 - map.getZoom()))),
      newlat = marker.getPosition().lat() + (0.000003 * Math.pow(2, (21 - map.getZoom())));

    if (v[0] === 'house') {
      newlong = marker.getPosition().lng() + (0.000052 * Math.pow(2, (21 - map.getZoom())));
      newlat = marker.getPosition().lat() + (0.00001 * Math.pow(2, (21 - map.getZoom())));
    }
    var infoBubble = new InfoBubble({
      map: map,
      content: '<div class="bubblewrap"><div class="bubble-arrow"></div><div class="bubble-info" style="">' + v[3] + '</div></div>',
      position: new google.maps.LatLng(newlat, newlong),
      shadowStyle: 0,
      padding: 0,
      borderRadius: 0,
      arrowSize: 7,
      fontFamily: 'Open Sans',
      borderWidth: 0,
      maxWidth: 125,
      minWidth: 125,
      minHeight: 36,
      maxHeight: 36,
      backgroundColor: 'transparent',
      disableAutoPan: true,
      hideCloseButton: true,
      arrowPosition: 30,
      backgroundClassName: 'bubble',
      arrowStyle: 2,
      disableAnimation: true
    });

    google.maps.event.addListener(marker, 'mouseover', function () {
      infoBubble.open();
      marker.setIcon(pahover);
    });

    google.maps.event.addListener(marker, 'mouseout', function () {
      infoBubble.close();
      marker.setIcon(pa);
    });

    google.maps.event.addListener(map, "zoom_changed", function () {
      if (v[0] === 'house') {
        newlong = marker.getPosition().lng() + (0.000052 * Math.pow(2, (21 - map.getZoom())));
        newlat = marker.getPosition().lat() + (0.00001 * Math.pow(2, (21 - map.getZoom())));
      } else {
        newlong = marker.getPosition().lng() + (0.000048 * Math.pow(2, (21 - map.getZoom())));
        newlat = marker.getPosition().lat() + (0.000003 * Math.pow(2, (21 - map.getZoom())));
      }
      infoBubble.setPosition(new google.maps.LatLng(newlat, newlong));
    });
  });
}

//// MOBILE FILTERS

// filters close
$(document).on('click', '.filter-block .button', function (e) {
  "use strict";
  $('#flats').toggleClass('filtered');
  var areafrom = $('.area .filter-from span.selected').text(),
    areato = $('.area .filter-to span.selected').text(),
    roomsfrom = $('.rooms .filter-from span.selected').text(),
    roomsto = $('.rooms .filter-to span.selected').text();
  if (areafrom === areato) {
    area = (areafrom + ' m\u00B2, ');
  } else {
    area = (areafrom + '-' + areato + ' m\u00B2, ');
  }
  if (roomsfrom === roomsto) {
    rooms = (roomsfrom + ' k.');
  } else {
    rooms = (roomsfrom + '-' + roomsto + 'k.');
  }
  $('.button span').text('Keisti filtravimą (' + area + rooms + ')');
  $('.flat-row').removeClass('filtered-out');
  $('.space-cont').each(function () {
    var space = $(this).text();
    if (!$(this).parent().parent().parent().is('.flat-table')) {
      if (space > areato || space < areafrom) {
        $(this).parent().parent().parent().addClass('filtered-out');
      }
    }
  });
  $('.rooms-cont').each(function () {
    var rooms = $(this).text();
    if (!$(this).parent().parent().parent().is('.flat-table')) {
      if (rooms > roomsto || rooms < roomsfrom) {
        $(this).parent().parent().parent().addClass('filtered-out');
      }
    }
  });
});

$(document).on('click', '.filter-back', function (e) {
  "use strict";
  $('#flats').toggleClass('filtered');
});

$(document).on('click', '.filter-less', function () {
  "use strict";
  var amount = $(this).next().find('span').length,
    areato = $('.area .filter-to span.selected').text(),
    areafrom = $('.area .filter-from span.selected').text(),
    roomsto = $('.rooms .filter-to span.selected').text(),
    roomsfrom = $('.rooms .filter-from span.selected').text(),
    sel = $(this).next().find('.selected');
  if ((sel.index()) < amount && sel.index() > 0) {
    if ($(this).parent().parent().parent().hasClass('area')) {
      if ($(this).parent().parent().hasClass('filter-from') && areato >= sel.text()) {
        sel.prev('span').addClass('selected');
        sel.removeClass('selected');
      } else if ($(this).parent().parent().hasClass('filter-to') && areafrom < sel.text()) {
        sel.prev('span').addClass('selected');
        sel.removeClass('selected');
      }
    } else if ($(this).parent().parent().parent().hasClass('rooms')) {
      if ($(this).parent().parent().hasClass('filter-from') && roomsto >= sel.text()) {
        sel.prev('span').addClass('selected');
        sel.removeClass('selected');
      } else if ($(this).parent().parent().hasClass('filter-to') && roomsfrom < sel.text()) {
        sel.prev('span').addClass('selected');
        sel.removeClass('selected');
      }
    }
  }
  //  disabler();
});

$(document).on('click', '.filter-more', function () {
  "use strict";
  var amount = $(this).prev().find('span').length,
    areato = $('.area .filter-to span.selected').text(),
    areafrom = $('.area .filter-from span.selected').text(),
    roomsto = $('.rooms .filter-to span.selected').text(),
    roomsfrom = $('.rooms .filter-from span.selected').text(),
    sel = $(this).prev().find('.selected');
  if ((sel.index() + 1) < amount && sel.index() >= 0) {
    if ($(this).parent().parent().parent().hasClass('area')) {
      if ($(this).parent().parent().hasClass('filter-from') && areato > sel.text()) {
        sel.next('span').addClass('selected');
        sel.removeClass('selected');
      } else if ($(this).parent().parent().hasClass('filter-to') && areafrom <= sel.text()) {
        sel.next('span').addClass('selected');
        sel.removeClass('selected');
      }
    } else if ($(this).parent().parent().parent().hasClass('rooms')) {
      if ($(this).parent().parent().hasClass('filter-from') && roomsto > sel.text()) {
        sel.next('span').addClass('selected');
        sel.removeClass('selected');
      } else if ($(this).parent().parent().hasClass('filter-to') && roomsfrom <= sel.text()) {
        sel.next('span').addClass('selected');
        sel.removeClass('selected');
      }
    }
  }
  //  disabler();
});

function disabler() {
  'use strict';
  var areafromind = $('.area .filter-from span.selected').index(),
    areatoind = $('.area .filter-to span.selected').index(),
    roomsfromind = $('.rooms .filter-from span.selected').index(),
    roomstoind = $('.rooms .filter-to span.selected').index(),
    areaitems = $('.area .filter-from span').length,
    roomsitems = $('.rooms .filter-from span').length,
    areato = $('.area .filter-to span.selected').text(),
    areafrom = $('.area .filter-from span.selected').text(),
    roomsto = $('.rooms .filter-to span.selected').text(),
    roomsfrom = $('.rooms .filter-from span.selected').text();
  $('.filter-to span, .filter-from span').removeClass('disabled');
  if (areato === areafrom) {
    $('.area .filter-from .filter-more, .area .filter-to .filter-less').addClass('disabled');
  } else {
    $('.filter-to span, .filter-from span').removeClass('disabled');
    if (areafromind === 0) {
      $('.area .filter-from .filter-less').addClass('disabled');
    } else if (areafromind + 1 === areaitems) {
      $('.area .filter-from .filter-more').addClass('disabled');
    } else {
      $('.area .filter-from .filter-less, .area .filter-from .filter-more').removeClass('disabled');
    }
  }
  if (areatoind === 0) {
    $('.area .filter-to .filter-less').addClass('disabled');
  } else if (areatoind + 1 === areaitems) {
    $('.area .filter-to .filter-more').addClass('disabled');
  } else {
    $('.area .filter-to .filter-less, .area .filter-to .filter-more').removeClass('disabled');
  }
  if (roomsfromind === 0) {
    $('.rooms .filter-from .filter-less').addClass('disabled');
  } else if (roomsfromind + 1 === roomsitems) {
    $('.rooms .filter-from .filter-more').addClass('disabled');
  }
  if (roomstoind === 0) {
    $('.rooms .filter-to .filter-less').addClass('disabled');
  } else if (roomstoind + 1 === roomsitems) {
    $('.rooms .filter-to .filter-more').addClass('disabled');
  }
}

// mobile feature accordion
$(document).on('touchend', '.feature', function () {
  "use strict";
  var touchend = new Date().getTime();
  if ((touchend - touchstart) < 150) {
    $(this).find('.feature-desc').slideToggle(200, function () {
      bodyscroll.refresh();
    });
    $(this).toggleClass('active');
  }
  $(this).toggleClass('active');
});

// mobile flat accordion
$("#flats .row").click(function () {
  "use strict";
  $(this).find('.flat-content').slideToggle();
  $(this).toggleClass('active');
});

// side menu open
$(document).on('click', '.sidemenu, .nav-toggle', function (e) {
  "use strict";
  if ($('.sidemenu').hasClass('expanded')) {
    $('.sidemenu').removeClass('expanded');
    if ($('#nav-toggle').hasClass('active')) {
      $('#nav-toggle').removeClass('active');
    }
  } else {
    $('.sidemenu').addClass('expanded');
    $('#nav-toggle').addClass('active');
  }
});

// menu flat overlay toggle
$(document).on('click touchend', '.flat-toggle, .menu-flat, #build-path-1', function (e) {
  'use strict';
  // target element id
  if (winw < 769) {
    e.preventDefault();
    if ($(this).attr('href') !== undefined) {
      $(this).removeAttr('href');
    }
    touchtime();
    if (noScroll) {
      $('#flats').fadeIn(200);
    }
  } else {
    if ($(this).attr('href')) {
      location.href = $(this).attr('href');
    } else {
      location.href = window.location.origin;
    }
  }
});


// menu smooth scroll
$(document).on('click touchend', '.sidemenu ul li a', function (e) {
  'use strict';
  if ($('#mob-map').length) {
    window.history.back();
  }
//  pagenum = $(this).parent().index();
  var el = $(this).attr('href'),
    top = $(el).position().top;
  e.preventDefault();
  if (winw < 769) {
    bodyscroll.scrollToElement(el, 500, 0, -43);
  } else {
    bodyscroll.scrollToElement(el, 500, 0, 0);
  }
});

// keyboard navigation
if (winw > 768) {
  $(document).keydown(function (e) {
    'use strict';
    switch (e.which) {
    case 38: // up
      bodyscroll.prev();
      break;
    case 40: // down
      bodyscroll.next();
      break;
    default:
      return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });
}


// close flat items list 
if (winw > 768) {
  $(document).on('click', '.flats-list-close', function () {
    'use strict';
    parent.history.back();
  });
}

// floor plan click function 
$(document).on('click', '.floor-plan > svg > path, .flat-mark', function () {
  'use strict';
  if (!$(this).is('.sold')) {
    //location.href = window.location.origin + '/aleja/flat-a1-1.html';
    // EDITED BY AK
    location.href = $(this).attr('data-url');
  }
});

//$(document).on('mouseover', '.floor-plan > svg > g > path', function () {
//  'use strict';
//  var contw = $(this)[0].getBBox().width,
//    conth = $(this)[0].getBBox().height,
//    calcw = contw / 3 + $(this).position().left * 1.25,
//    calch = contw / 3 + $(this).position().top * 1.25,
//    tstring = 'translate(' + calcw + 'px, ' + calch + 'px)';
//  $(this).parent().find('.circlemarker').css({
//    "-webkiy-transform": tstring,
//    "-moz-transform": tstring,
//    "-o-transform": tstring,
//    "transform": tstring
//  });
//});

// menu flat overlay toggle
$(document).on('click', '.overlay-close', function (e) {
  'use strict';
  e.preventDefault();
  // target element id
  $('#flats').fadeOut(200);
  $('.sidemenu').removeClass('expanded');
  if ($('#nav-toggle').hasClass('active')) {
    $('#nav-toggle').removeClass('active');
  }
});

// top menu collapse
//$(document).scroll(function () {
bodyscroll.on('scrollEnd', function () {
  'use strict';
  if (winw < 769) {
    console.log(bodyscroll.y);
    if (bodyscroll.y < 0) {
      $('.mob-nav').stop().animate({
        'margin-top': '-10px'
      }, 200).removeClass('transparent');
      $('.nav-toggle').stop().animate({
        'padding-bottom': '10px'
      }, 200);
    } else {
      $('.mob-nav').stop().animate({
        'margin-top': '0px'
      }, 200).addClass('transparent');
      $('.nav-toggle').stop().animate({
        'padding-bottom': '20px'
      }, 200);
    }
  }
});



// filters open
$(document).on('click', '.flats .button', function (e) {
  "use strict";
  var height = $('.flats').height();
  //  disabler();
  $('.filtering').height(height);
  $('#flats').toggleClass('filtered');
});

// front building hovers
$(document).on('mouseover', '#build-path-a, .build-a', function (e) {
  "use strict";
  $('.build-button').removeClass('active');
  $('.build-a').addClass('active');
});

$(document).on('mouseover', '#build-path-b, .build-b', function (e) {
  "use strict";
  $('.build-button').removeClass('active');
  $('.build-b').addClass('active');
});

$(document).on('mouseover', '#build-path-c, .build-c', function (e) {
  "use strict";
  $('.build-button').removeClass('active');
  $('.build-c').addClass('active');
});
 // EDITED BY AK
$(document).on('click', '#build-path-a, .build-a', function (e) {
  "use strict";
  //location.href = window.location.origin + '/aleja/house.html';
  
  location.href = $(this).attr('data-url');
});

$(document).on('click touchend', '.gallery-navigation .arr-right', function () {
  'use strict';
  if (!scrolling) {
    slider();
    sliderName.next();
  }
});

$(document).on('click touchend', '.gallery-navigation .arr-left', function () {
  'use strict';
  if (!scrolling) {
    slider();
    sliderName.prev();
  }
});


$(document).on('click touchend', '#location .button', function (e) {
  'use strict';
  touchtime();
  if (noScroll) {
    location.href = window.location.origin + '/aleja/' + $(this).attr('href');
  } else {
    e.preventDefault();
  }
});

/*
$(document).on('touchend', 'body', function () {
  "use strict";
  slider();
});*/

//mobgallery.on('flick', function () {
//  "use strict";
//  slider();
//});

//function slider() {
//  "use strict";
//  if (winw > 769) {
//    sliderName = deskgallery;
//    slidecount = ($('.desk-gallery .scroller .slide').length);
//  } else {
//    sliderName = mobgallery;
//    slidecount = ($('.mob-gallery .scroller .slide').length);
//  }
//  slidenum = sliderName.currentPage.pageX;
//  $('.slide-total').empty().append(slidecount - 2);
//  //slidersize = $('.slide').width() * slidecount;
//  //$('.scroller').width(slidersize);
//  //sliderName.on('scrollEnd', function () {
//  //});
//}

function slider() {
  'use strict';
  if (winw > 769) {
    sliderName = deskgallery;
    slidecount = ($('.desk-gallery .scroller .slide').length);
  } else {
    sliderName = mobgallery;
    slidecount = ($('.mob-gallery .scroller .slide').length);
  }
  slidenum = sliderName.currentPage.pageX;
  $('.slide-total').empty().append(slidecount - 2);
  scrolling = true;
  //  console.log(sliderName.x);
  if (sliderName.x <= -sliderName.wrapperWidth * (slidecount - 1) + 10) {
    sliderName.goToPage(1, 0, 0);
    sliderName.on('scrollEnd', function () {
      $('.slide-current').empty().append('1');
      scrolling = false;
    });
  } else if (sliderName.x >= -1) {
    sliderName.goToPage(slidecount - 2, 0, 0);
    sliderName.on('scrollEnd', function () {
      $('.slide-current').empty().append(slidecount - 2);
      scrolling = false;
    });

  } else {
    sliderName.on('scrollEnd', function () {
      $('.slide-current').empty().append(slidenum);
      scrolling = false;
    });
  }

  console.log(scrolling);

  /*

    if (slidenum === slidecount - 1) {
      sliderName.goToPage(1, 0, 0);
      $('.slide-current').empty().append('1');
    } else if (slidenum === 0) {
      sliderName.goToPage(slidecount - 2, 0, 0);
      $('.slide-current').empty().append(slidecount - 2);
    } else {
      $('.slide-current').empty().append(slidenum);
    }*/
}

// function basically for calculating VH VW on doc ready ir on resize
function rescale() {
  "use strict";
  if (winw > 768) {
    $('.gallery-container, .slide').height(winh);
    $('.gallery-container, .slide').width(winw - 360);
  }
}


//// centering floor markers
//function setMarkers() {
//  "use strict";
//  $.each($('.floor-plan > svg > g > path.path-color'), function () {
//    //    var contw = $(this)[0].getBBox().width,
//    //      conth = $(this)[0].getBBox().height,
//    var contw = $(this)[0].getBoundingClientRect().width,
//      conth = $(this)[0].getBoundingClientRect().height,
//      calcw = contw / 2 / 1.5 + $(this).position().left * 1.3,
//      calch = contw / 2 / 1.5 + $(this).position().top * 1.3,
//      tstring = 'translate(' + calcw + 'px, ' + calch + 'px)';
//    $(this).parent().find('.circlemarker').css({
//      //      "opacity": 1,
//      "-webkiy-transform": tstring,
//      "-moz-transform": tstring,
//      "-o-transform": tstring,
//      "transform": tstring
//    }, 0).show();
//  });
//}

// centering floor markers
function setMarkers() {
  "use strict";
  var floors = [],
    status = 'free',
    x = 0,
    y = 0;
  //  state = free;
  free = 'url(#free)';
  sold = 'url(#sold)';
  reserved = 'url(#reserved)';
  hover = 'url(#hover)';
  $.each($('.flat-shape'), function () {
    floorid = $(this).attr('id').replace('flat-', '');
    if ($(this).is('.sold')) {
      status = 'sold';
      state = sold;
    } else if ($(this).is('.reserved')) {
      status = 'reserved';
      state = reserved;
    } else {
      status = 'free';
      state = free;
    }
    $(this).attr('fill', state);

    //    alert($(this).attr('fill'));
    x = $(this).position().left - $('.floor-plan svg').position().left;
    y = $(this).position().top - $('.floor-plan svg').position().top;
    //      calcw = ($('.floor-plan svg').width() / 9.6) + x;
    calcw = ($('.floor-plan svg').width() / 9) + x;
    calch = ($('.floor-plan svg').height() / 6) + y;
    floors.push(floorid, calcw, calch, status);
  });
  $.each($('.flat-mark'), function () {
    var markid = $(this).attr('id').replace('mark-', ''),
      index = floors.indexOf(markid, 0),
      centw = floors[index + 1],
      centh = floors[index + 2],
      status = floors[index + 3];
    $(this).css({
      top: centh,
      left: centw
    }).addClass(status);

  });
}

$(document).on('mouseenter', 'path', function () {
  "use strict";
  var markid = $(this).attr('id').replace('flat-', 'mark-'),
    marker = $(this).parent().parent().find('#' + markid);
  $('.flat-shape').attr('class', function (index, classNames) {
    return classNames.replace('hovered', '');
  });
  $(this).attr('class', function (index, classNames) {
    return classNames + ' hovered';
  });
  if (!$(this).is('.sold')) {
    $(this).attr('fill', 'url(#hover)');
  }
  $('.flat-mark').removeClass('hovered');
  marker.addClass('hovered');
});

$(document).on('mouseleave', 'path', function () {
  "use strict";
  if ($(this).is('.sold')) {
    state = sold;
  } else if ($(this).is('.reserved')) {
    state = reserved;
  } else {
    state = free;
  }
  $(this).attr('fill', state);
});


$(document).on('mouseenter', '.flat-mark', function () {
  "use strict";
  $('.flat-mark').removeClass('hovered');
  $(this).addClass('hovered');
});

$(document).on('mouseleave', '.flat-mark', function () {
  "use strict";
  $(this).removeClass('hovered');
});

$(window).resize(function () {
  "use strict";
  setMarkers();
  rescale();
});

// building page hovers for floor selection
$(document).on('mouseover', '#floor-selection polygon', function () {
  'use strict';
  floorid = $(this).attr('id').replace("polyfloor", "");
  $('.polyfloor-pop').hide();
  var popid = $('.pop' + floorid);
  popid.addClass('active');
  popid.show();
});


// floor stripe hovers for floor selection
$(document).on('mouseover', '.floor-sel', function () {
  'use strict';
  if ($(this).parent().parent().hasClass('house')) {
    floorid = $(this).attr('id').replace("floorsel", "");
    $('.polyfloor-pop').hide();
    var popid = $('.pop' + floorid);
    //  popid.addClass('active');
    popid.show();
  }
});

//$(document).on('click', '#floor-selection polygon', function () {
//  'use strict';
//  location.href = $(this).attr('href');
//});


/*
// EDITED BY AK
// house - floor click function 
$(document).on('click', '.floor-selection .shape', function (e) {
  'use strict';
  e.preventDefault();
  var floorid = $(this).attr('id').replace('polyfloor-', '');
  location.href = window.location.origin + '/aleja/floor-' + floorid + '.html';
  //    location.href = window.location.origin + '/floor-' + floorid + '.html';
});
*/
$(document).on('click', '.floor-selection .shape', function (e) {
  'use strict';  
  location.href = $(this).attr('href');
});

/*
// EDITED BY AK
$(document).on('click', '.floor-sel', function (e) {
  'use strict';
  e.preventDefault();
  var floorid = $(this).attr('id').replace('floorsel-', '');
  location.href = window.location.origin + '/aleja/floor-' + floorid + '.html';
  //    location.href = window.location.origin + '/floor-' + floorid + '.html';
});
*/

// menu flat overlay toggle
$(document).on('click', '.house-overlay-close', function () {
  'use strict';
    location.href = window.location.origin;
//  parent.history.back();
});

//// scroll to contacts
//$(document).on('click', 'span.contact.button', function (e) {
//  'use strict';
//  e.preventDefault();
//  bodyscroll.goToPage(0, 5, 1500);
//});


// day toggle
$(document).on('click', '.switch', function () {
  'use strict';
  $(this).toggleClass('night');
  if ($(this).hasClass('night')) {
    $('.daytime').empty().append('Naktis');
    $('#day-img').fadeIn(200);
    $('#night-img').fadeOut(200);
  } else {
    $('.daytime').empty().append('Diena');
    $('#day-img').fadeOut(200);
    $('#night-img').fadeIn(200);
  }
});

// 3D view switch
$(document).on('click', '.view-switch', function () {
  'use strict';
  $(this).toggleClass('active');
  if (!$(this).is('.active')) {
    $('.twod-img').fadeOut(100, function () {
      $('.threed-img').fadeIn(100);
    });
  } else {
    $('.threed-img').fadeOut(100, function () {
      $('.twod-img').fadeIn(100);
    });

  }
});

// right sidebar open + show similar
$(document).on('click', '.similar-btn', function () {
  'use strict';
  $('.contact-broker').hide();
  $('.similar').show();
  $('#landing').addClass('slideleft');
});

// right sidebar open + show broker
$(document).on('click', '.contact-broker-btn, span.contact.button', function () {
  'use strict';
  $('.similar').hide();
  $('.contact-broker').show();
  $('#landing').addClass('slideleft');
});

// right sidebar close
$(document).on('click', '.close-side', function () {
  'use strict';
  $('#landing').removeClass('slideleft');
});

// filter dropdown open 
$(document).on('mouseenter', '.filter-group', function () {
  'use strict';
  $(this).find('.filter-dropdown').addClass('open');
});

// filter dropdown close 
$(document).on('mouseleave', '.filter-group', function () {
  'use strict';
  $(this).find('.filter-dropdown').removeClass('open');
});

// filter dropdown close 
$(document).on('touchstart', '.filter-value', function () {
  'use strict';
  $('.filter-group').removeClass('open');
  $(this).next().addClass('open');
});

// desktop filtering
$(document).on('click', '.filter-group li', function () {
  'use strict';
  //  $(this).parent().parent().parent().find('.filter-dropdown').removeClass('open');
  //  $(this).parent().parent().parent().find('.filter-caret').removeClass('open');
  $(this).addClass('selected');
  filterval = $(this).find('span').text();
  $(this).parent().parent().parent().find('.filter-value span').empty().append(filterval);
  $('.flat-row').removeClass('filtered-out');
  var status = $('.flat-status .filter-value span').text(),
    pricefrom = $('.flat-price-from .filter-value span').text(),
    priceto = $('.flat-price-to .filter-value span').text(),
    area = $('.flat-area .filter-value span').text();

  $('.flat-price-from li').each(function () {
    $(this).show();
    if (($(this).find('span').text() / 1000) > (priceto / 1000)) {
      $(this).hide();
    }
  });

  $('.flat-price-to li').each(function () {
    $(this).show();
    if (($(this).find('span').text() / 1000) < (pricefrom / 1000)) {
      $(this).hide();
    }
  });

  $('.flat-row').each(function () {
    var rowprice = $(this).find('.price-cont').text(),
      rowarea = $(this).find('.space-cont').text();
    //    $(this).removeClass('filtered-out');
    $(this).slideDown();
    //    $(this).show();
    if (status === 'Laisvus' && !$(this).is('.free')) {
      //      $(this).addClass('filtered-out');
      $(this).slideUp();
      //      $(this).fadeout(150);
    } else if (status === 'Rezervuotus' && !$(this).is('.reserved')) {
      //      $(this).addClass('filtered-out');
      $(this).slideUp();
      //      $(this).fadeOut(150);
    }
    if (pricefrom !== 'Nuo' && pricefrom / 10000 > rowprice / 10000) {
      $(this).slideUp();
      //      $(this).fadeOut(150);
    } else if (priceto !== 'Iki' && priceto / 10000 < rowprice / 10000) {
      $(this).slideUp();
      //      $(this).fadeOut(150);
    }
    if (area > rowarea) {
      $(this).slideUp();
      //      $(this).fadeOut(150);
    }
  });

  setTimeout(function () {
    if ($('.flat-row:hidden').length === $('.flat-row').length) {
      $('.flat-table-error').show();
    } else {
      $('.flat-table-error').hide();
    }
  }, 801);

  $('.eurosign').remove();
  $('.pre').remove();
  if (pricefrom !== 'Nuo') {
    $('.flat-price-from .filter-value').append('<i class="eurosign">&nbsp;&euro;</i>');
    $('.flat-price-from .filter-value').prepend('<i class="pre">Nuo&nbsp;</i>');
  }
  if (priceto !== 'Iki') {
    $('.flat-price-to .filter-value').append('<i class="eurosign">&nbsp;&euro;</i>');
    $('.flat-price-to .filter-value').prepend('<i class="pre">Iki&nbsp;</i>');
  }
  if (is_touch) {
    $(this).parent().parent().removeClass('open');
  }
});

$(document).on('click', '.desk-filter-btn', function () {
  'use strict';
  var parkPrice = 3000;
  //  if ($(this).text() === 'Rodyti kainą su parkavimu') {
  if (!$(this).hasClass('active')) {
    $('.flat-row .price-cont').each(function () {
      var rowprice = $(this).text();
      $(this).empty().append(parseInt(rowprice, 10) + parkPrice);
    });
    $(this).text('Rodyti kainą be parkavimo').addClass('active');
  } else {
    $('.flat-row .price-cont').each(function () {
      var rowprice = $(this).text();
      $(this).empty().append(parseInt(rowprice, 10) - parkPrice);
    });
    $(this).text('Rodyti kainą su parkavimu').removeClass('active');
  }
});


function ie() {
  'use strict';
  ms_ie = false;
  ua = window.navigator.userAgent;
  old_ie = ua.indexOf('MSIE ');
  new_ie = ua.indexOf('Trident/');
  if ((old_ie > -1) || (new_ie > -1)) {
    ms_ie = true;
  }
}

//svg scaler

function scaleSVG() {
  'use strict';
  $('.front-image').each(function () {
    var svgWidth = $(this).find("svg").attr("width"),
      svgHeight = $(this).find("svg").attr("height"),
      thisHeight = $(this).height(),
      scale = svgHeight / thisHeight,
      thisWidth = parseInt((svgWidth / scale) + 3, 10);
    $(this).width(thisWidth);
  });
}

function scrolls() {
  'use strict';
  if (typeof bodyscroll !== 'undefined') {
    bodyscroll.destroy();
  }
  if (typeof mobgallery !== 'undefined') {
    mobgallery.destroy();
  }
  if (typeof deskgallery !== 'undefined') {
    deskgallery.destroy();
  }
  var bodysnap = true;
  if (winw < 769 || $('#flat-list').length) {
    bodysnap = false;
  }
  if ($('.mob-gallery').length) {
    mobgallery = new IScroll('.mob-gallery', {
      scrollX: true,
      scrollY: false,
      eventPassthrough: true,
      snap: true,
      momentum: false,
      probeType: 3
    });
    mobgallery.on('scroll', slider);
    if ($('.mob-gallery').is(':visible')) {
      mobgallery.goToPage(1, 0, 0);
    }
  }
  if ($('.desk-gallery').length) {
    rescale();
    deskgallery = new IScroll('.desk-gallery', {
      scrollX: true,
      scrollY: false,
      disableMouse: true,
      mouse: false,
      momentum: false,
      disablePointer: true,
      snap: true,
      probeType: 3
    });
    deskgallery.on('scroll', slider);
    if ($('.desk-gallery').is(':visible')) {
      deskgallery.goToPage(1, 0, 0);
    }
    slidecount = ($('.desk-gallery .scroller .slide').length);
    $('.slide-total').empty().append(slidecount - 2);
  }
  if ($('.bodyscroll').length) {
    scroll = false;
    flatscroll = false;
    mobease = false;
    if ($('#flat-list').length) {
      flatscroll = true;
    }
    bodyscroll = new IScroll('.bodyscroll', {
      scrollX: false,
      scrollY: true,
      mouseWheel: true,
//      mouseWheel: flatscroll,
//      snap: bodysnap,
      snap: false,
      disableMouse: true,
      mouse: false,
      momentum: !bodysnap
    });
    setTimeout(function () {
      bodyscroll.refresh();
    }, 300);
    bodyscroll.on('scrollEnd', function () {
      scroll = false;
    });
//    if (!flatscroll) {
//      $('.bodyscroll').bind('DOMMouseScroll mousewheel', function (e) {
//        currpage = bodyscroll.currentPage.pageY;
//        if (e.type === 'mousewheel') {
//          scrollwheel = (e.originalEvent.wheelDelta * -1);
//        } else if (e.type === 'DOMMouseScroll') {
//          scrollwheel = 40 * e.originalEvent.detail;
//        }
//        if (!scroll || currpage === 0 || !bodysnap) {
//          if (scrollwheel / 120 < 0) {
//            scroll = true;
//            bodyscroll.goToPage(0, currpage - 1, 500);
//          } else {
//            if (currpage !== 4) {
//              scroll = true;
//              bodyscroll.goToPage(0, currpage + 1, 500);
//            }
//          }
//        }
//      });
//    }
    setMarkers();
  }
}

$(document).on('click touchend', '.back-to-floor', function (e) {
  'use strict';
  e.preventDefault();
  window.history.back();
});


$(document).on('touchstart', 'html', function () {
  'use strict';
  touchstart = new Date().getTime();
});


$(document).on('touchend', 'html', function () {
  'use strict';
  var touchend = new Date().getTime();
});

function touchtime() {
  'use strict';
  var touchend = new Date().getTime();
  if ((touchend - touchstart) < 150) {
    noScroll = true;
  } else {
    noScroll = false;
  }
}
