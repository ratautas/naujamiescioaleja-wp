<?php
get_header();
get_template_part( 'subheader' );

  if ( have_posts() ){ while ( have_posts() ) : the_post(); ?>

<div id="landing" class="section">
          <div class="house if-desk">
            <div class="column">
              <div class="house-actions">
                <div class="house-overlay-close">
<!--
                  <span>
                  <svg width="15.556" height="15.556" viewBox="0 0 15.556 15.557">
                    <g>
                      <path d="m2.417 1.002l14.142 14.143-1.414 1.414-14.142-14.143 1.414-1.414z"/>
                      <path d="m16.559 2.416l-14.142 14.143-1.414-1.414 14.142-14.143 1.414 1.414z"/>
                    </g>
                    </svg>
                  </span>
-->
                  <?php _e(' Uždaryti', 'theme_site_domainname'); ?>
                </div>
                <div class="house-navigation">
                  <a href="<?php the_field('house_link_left'); ?>" ><div class="house-prev-arr">
<!--
                    <svg height="14" viewBox="0 0 8 14" width="8">
                      <path d="m6.4824 0.0019531 1.5176 1.4042-5.1816 5.5938 5.1816 5.596-1.5176 1.404-6.4824-7 6.4824-6.998z" />
                    </svg>
-->
                  </div>
                  </a>
                  <div class="house-title"><?php the_title(); ?></div>
                  <a href="<?php the_field('house_link_right'); ?>"><div class="house-next-arr">
<!--
                    <svg height="14" viewBox="0 0 8 14" width="8">
                      <path d="m1.5176 0.0019531-1.5176 1.4042 5.1816 5.5938-5.1816 5.596 1.5176 1.404 6.4824-7-6.4824-6.998z" />
                    </svg>
-->
                  </div>
                 </a>
                </div>
              </div>
              <div class="house-text">
              	<?php the_content(); ?>               	
              </div>
              <div class="house-action-text"><?php _e('Pasirinkite aukštą, kad susipažint su butų išdėstymu.', 'theme_site_domainname'); ?>
              </div>
              <span class="button contact"><?php _e('Susisiekti su vadybininku', 'theme_site_domainname'); ?></span>
            </div>
            <?php 
                	$h_location_house = get_field('h_location_house');
                	
          			$floors = array();
                	
                	while (have_rows('floor_menu')) {
           					the_row();
							
							$a = get_sub_field('h_floor_name');
							$b = get_sub_field('h_floor_link');
							$c = get_sub_field('h_floor_overlay_id');
							$floors[$c]['location'] = $a;
							$floors[$c]['link'] = $b;
							$floors[$c]['overlay'] = $c;
			
        			}
					
					
					
			?> 
			<div class="floor-stripe">
              <span class="floor-label"><?php _e('AUKŠTAS', 'theme_site_domainname'); ?></span>
              <?php
              foreach ($floors as $value) {
    				
				  	echo '<a class="floor-sel" href="' . $value['link'] . '" id="floorsel-a' . $value['overlay'] . '">' . $value['location'] . '</a>';
				}
				
			  ?>
            </div>
            <div class="right">
              <div class="floor-selection" id="house01">
                <svg id="floor-selection" height="100%" class="floors-svg" viewBox="0 0 3067.5 2108.75" version="1.1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet">
                  <polygon href="<?php if(isset($floors['9']['link'])) echo $floors['9']['link']; ?>" id="polyfloor-a9" points="151.0991973877 348.2796936035, 829.6162872314 136.6430664063, 1459.2133941650 559.6302795410, 1456.8597941399 736.0265960693, 829.6323161125 396.6482391357, 149.7802653313 563.0351715088, 151.0727853775 350.6938476563" class="shape" fill="transparent"></polygon>
                  <polygon href="<?php if(isset($floors['8']['link'])) echo $floors['8']['link']; ?>" id="polyfloor-a8" points="151.0991973877 708.2796630859, 829.6162872314 560.6430358887, 1459.2133941650 869.6302490234, 1456.8597941399 736.0265655518, 829.6323161125 396.6482086182, 149.7802653313 563.0351409912, 151.0727853775 710.6938171387" class="shape" fill="transparent"></polygon>
                  <polygon href="<?php if(isset($floors['7']['link'])) echo $floors['7']['link']; ?>" id="polyfloor-a7" points="151.0991973877 708.2796630859, 829.6162872314 560.6430358887, 1459.2133941650 869.6302490234, 1456.8597412109 988.0266113281, 829.6323242188 726.6481933594, 149.7802734375 863.0351562500, 151.0727844238 710.6938476563" class="shape" fill="transparent"></polygon>
                  <polygon href="<?php if(isset($floors['6']['link'])) echo $floors['6']['link']; ?>" id="polyfloor-a6" points="151.0991973877 1000.2797241211, 829.6162872314 896.6430816650, 1459.2133941650 1103.6303253174, 1456.8597941399 988.0266265869, 829.6323161125 726.6482391357, 149.7802653313 863.0351715088, 151.0727853775 1002.6938171387" class="shape" fill="transparent"></polygon>
                  <polygon href="<?php if(isset($floors['5']['link'])) echo $floors['5']['link']; ?>" id="polyfloor-a5" points="151.0991973877 1000.2797241211, 829.6162872314 896.6430816650, 1459.2133941650 1103.6303253174, 1456.8597941399 1226.0266265869, 829.6323161125 1060.6482238770, 149.7802653313 1147.0352249146, 151.0727853775 1002.6938247681" class="shape" fill="transparent"></polygon>
                  <polygon href="<?php if(isset($floors['4']['link'])) echo $floors['4']['link']; ?>" id="polyfloor-a4" points="151.0991973877 1292.2796630859, 829.6162872314 1232.6430625916, 1459.2133941650 1347.6302604675, 1456.8597941399 1226.0265617371, 829.6323161125 1060.6481590271, 149.7802653313 1147.0351600647, 151.0727853775 1294.6937599182" class="shape" fill="transparent"></polygon>
                  <polygon href="<?php if(isset($floors['3']['link'])) echo $floors['3']['link']; ?>" id="polyfloor-a3" points="151.0991973877 1292.2796630859, 829.6162872314 1232.6430625916, 1459.2133941650 1347.6302604675, 1456.8597941399 1466.0265617371, 829.6323161125 1400.6481590271, 149.7802653313 1431.0351581573, 151.0727853775 1294.6937580109" class="shape" fill="transparent"></polygon>
                  <polygon href="<?php if(isset($floors['2']['link'])) echo $floors['2']['link']; ?>" id="polyfloor-a2" points="151.0991973877 1572.2796630859, 829.6162872314 1570.6430630684, 1459.2133941650 1583.6302628517, 1456.8597941399 1466.0265641212, 829.6323161125 1400.6481614113, 149.7802653313 1431.0351605415, 151.0727853775 1574.6937603951" class="shape" fill="transparent"></polygon>
                  <polygon href="<?php if(isset($floors['1']['link'])) echo $floors['1']['link']; ?>" id="polyfloor-a1" points="151.0991973877 1572.2796630859, 829.6162872314 1570.6430630684, 1459.2133941650 1583.6302628517, 1456.8597941399 1704.0265641212, 829.6323161125 1730.6481633186, 149.7802653313 1719.0351634026, 151.0727853775 1574.6937632561" class="shape" fill="transparent"></polygon>
                  <polygon href="<?php if(isset($floors['0']['link'])) echo $floors['0']['link']; ?>" id="polyfloor-a0" points="151.0991973877 1812.2796630859, 829.6162872314 1862.6430625916, 1459.2133941650 1737.6302604675, 1456.8597941399 1704.0265617371, 829.6323161125 1730.6481609344, 149.7802653313 1719.0351610184, 151.0727853775 1814.6937608719" class="shape" fill="transparent"></polygon>
                </svg>
                 <?php
              	foreach ($floors as $value) { ?>
    				
				
				  	<div class="polyfloor-pop pop-a<?php echo $value['overlay'] ; ?>">
                  <p><?php _e('Laisvi: ', 'theme_site_domainname'); ?><span class="free-flats"></span>
                  </p>
                  <p><?php _e('Rezervuota: ', 'theme_site_domainname'); ?><span class="reserved-flats"></span>
                  </p>
                </div>
				  	
				  	<?php
				}							
				
			  ?>
                               
                
              </div>
              <div class="floor-page">
                <div id="floorA9">
                  <div class="floor-plan">
                    <svg xmlns="http://www.w3.org/2000/svg" height="520" width="715" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 715.00002 519.99999">
                      <defs>
                        <pattern id="overlay" patternUnits="userSpaceOnUse" width="715" height="520" y="754.51" x="278.84">
                          <image width="715" height="520" xlink:href="img/demo-flat.png" y="0" x="0" preserveAspectRatio="none" />
                        </pattern>
                      </defs>
                      <g transform="translate(-278.84 -754.51)">
                        <image width="715" xlink:href="img/demo-flat.png" height="520" y="754.51" x="278.84" preserveAspectRatio="none" />
                        <path class="reserved" d="m 694.16001,1234.9 0,-191 -160.46002,0 0,191.27 z" />
                        <path fill="url(#overlay)" d="m696.16 1236.9v-195h-164.46v195.27z" />
                      </g>
                    </svg>
                  </div>
                  aaaaa
                </div>
              </div>
            </div>
          </div>
          <div id="slide-overlay">
            <div class="slide-side">
              <div class="close-side">                
<!--
                  <svg width="15.556" height="15.556" viewBox="0 0 15.556 15.557">
                    <g>
                      <path d="m2.417 1.002l14.142 14.143-1.414 1.414-14.142-14.143 1.414-1.414z"/>
                      <path d="m16.559 2.416l-14.142 14.143-1.414-1.414 14.142-14.143 1.414 1.414z"/>
                    </g>
                    </svg>                
-->
              </div>
              <?php 
              
              $post_object = get_field('related_contact');
				
				if( $post_object ): 

					$post = $post_object;
					setup_postdata( $post ); 
					
					
					?>
              <div class="contact-broker">
                <h3><?php _e('Susisiekite', 'theme_site_domainname'); ?></h3>
                <div class="broker">
                  <img src="<?php the_field('image'); ?>" alt="<?php the_field('name'); ?>" />
                  <h5 class="brokername"><?php the_field('name'); ?></h5>
                  <span class="brokerinfo">
<?php the_field('phone'); ?><br />
<?php the_field('email'); ?>           
            </span>
                </div>
                <form id="brokerform" name="brokerform">
                  <input type="text" id="brokername" name="brokername" placeholder="<?php _e('Vardas', 'theme_site_domainname'); ?>">
                  <input type="text" id="brokeremail" name="brokeremailemail" placeholder="<?php _e('Telefonas arba el. paštas', 'theme_site_domainname'); ?>">
                  <textarea id="brokermessage" name="brokermessagemessage" placeholder="<?php _e('Pranešimas', 'theme_site_domainname'); ?>"></textarea>
                  <input type="button" id="brokersubmit" class="button" value="<?php _e('Siųsti', 'theme_site_domainname'); ?>">          
                </form>
              </div>
            </div>
          </div>
        </div>
<?php 
wp_reset_postdata();
endif;
endwhile;}?> 
<?php get_footer(); ?>
