
  <div class="design">
    <!--    <img src="img/screens/desktop/01_homepage.jpg" alt="desktop">-->
    <!--    <img src="img/screens/desktop/10_menu.jpg" alt="desktop">-->
    <!--    <img src="img/screens/mobile/01_homepage.jpg" alt="mobile">  -->
  </div>
  <div class="wrapper">
    <div class="nav">
      <nav>
        <div class="mob-nav transparent">
          <a class="small-logo" href="<?php echo home_url(); ?>"></a>
          <div class="lang">
          	<?php
          	global $suffix;
                        if ($suffix == '-lt') {

                            echo '<a href="' .esc_url(network_site_url( '/ru/' )) . '">RU</a>';			
							
                        } else {						
                            echo '<a href="' .esc_url(network_site_url( '/lt/' )) . '">LT</a>';
                        }                         
              ?> 
          	
          </div>
          <div class="nav-toggle">
            <span class="nav-bar"></span>
            <span class="nav-bar"></span>
            <span class="nav-bar"></span>
          </div>
        </div>
        <div class="sidemenu">
          <div class="menu-close"></div>
          <ul>
            <li><a href="#landing" class="menu-flat"><?php _e('BUTAI', 'theme_site_domainname'); ?></a>
            </li>
            <li><a href="#about"><?php _e('APIE PROJEKTĄ', 'theme_site_domainname'); ?></a>
            </li>
            <li><a href="#location"><?php _e('VIETA MIESTE', 'theme_site_domainname'); ?></a>
            </li>
            <li><a href="#gallery"><?php _e('GALERIJA', 'theme_site_domainname'); ?></a>
            </li>
            <li><a href="#contacts"><?php _e('KONTAKTAI', 'theme_site_domainname'); ?></a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
    
<div class="bodyscroll">
      <div class="bodyscroller">