
<?php 

wp_reset_query();
global $suffix;
$args = array(  
'name' => 'config-footer' . $suffix,
'post_type' => 'site_config'
);
$the_query = new WP_Query($args);
if ($the_query->have_posts()) { while ($the_query->have_posts()) {  $the_query->the_post();      
	
?>

 <div id="about" class="section">
          <div class="column">
            <h2><?php _e('APIE NAUJAMIESČIO ALĖJĄ', 'theme_site_domainname'); ?></h2>
            <div class="details if-mob">              
              <?php                 	
                	while (have_rows('about_features')) {
           					the_row();
						 ?>
						<div class="feature">
                <div class="feature-head">
                  <h3><?php the_sub_field('feature_title'); ?></h3><i class="feature-toggle"></i>
                </div>
                <div class="feature-desc">
                  <p><?php the_sub_field('feature_text'); ?></p>
                </div>
              </div>		
						<?php				
        			}			
			?>
            </div>
            <a class="button apt-list if-desk" href="<?php the_field('search_page_link'); ?>"><?php _e('Butų kainos ir užimtumas', 'theme_site_domainname'); ?></a>
          </div>
          <div class="right if-desk">
            <div class="desk-features">
              <div class="feature-scroll">                
                 <?php 
                	
                	while (have_rows('about_features')) {
           					the_row();
						 ?>		
							<div class="feature-row">
                  <div class="feature-icon"><img src="<?php the_sub_field('feature_img'); ?>" alt="" />
                  </div>
                  <h4 class="feature-title"><?php the_sub_field('feature_title'); ?></h4>
                  <div class="feature-text"><?php the_sub_field('feature_text'); ?>
                  	
                  </div>
                </div>							
						<?php	
			
        			}
			
			?>
			  </div>
            </div>
          </div>
        </div>
        <div id="location" class="section">
          <div class="column">
            <h2><?php _e('VIETA MIESTE', 'theme_site_domainname'); ?></h2>
            <div class="column-text">
              <p><?php the_field('map_text'); ?></p>
              <a class="button if-mob" href="<?php the_field('map_page_link'); ?>"><?php _e('Rasti butą', 'theme_site_domainname'); ?></a>
            </div>
          </div>
          <div class="right if-desk">
            <div class="map">
              <div id="map-canvas">
              </div>
            </div>
          </div>
        </div>
          <script>
          <?php 
          $places2 = array();
          if(have_rows('map_markers')) {
		        while (have_rows('map_markers')) {
		            the_row();
					$places2[] = array(get_sub_field('marker_type'), get_sub_field('marker_latitude'), get_sub_field('marker_longitude'), get_sub_field('marker_title')); 
		        }
			}
          
          
          ?>
    var latitude = '<?php the_field('map_latitude'); ?>';
    var longitude = '<?php the_field('map_longitude'); ?>';
    var places = <?php echo json_encode($places2); ?>;    
  </script>
        <div id="gallery" class="section">
          <div class="column">
            <h2><?php _e('GALERIJA', 'theme_site_domainname'); ?></h2>
            <div class="column-text">
              <p>
              	<?php the_field('gallery_text'); ?>
              </p>
            </div>
            <div class="gallery if-mob">
              <div class="gallery-container mob-gallery">
                <div class="scroller">                	
                	<?php 
                	
                	while (have_rows('gallery')) {
           					the_row();
							echo '<div class="slide" style="background-image: url('. get_sub_field('gallery_img') . ')"></div>';
			
        			}
					?>                	
                </div>
              </div>
              <div class="gallery-navigation">
                <span class="arr-left"></span>
                <span class="slidecount">
              <span class="slide-current">1</span><span>&nbsp;/&nbsp;</span><span class="slide-total"></span>
                </span>
                <span class="arr-right"></span>
              </div>
            </div>
          </div>
          <div class="right">
            <div class="gallery if-desk">
              <div class="gallery-container desk-gallery">
                <div class="scroller">
                	<?php   
                	        $firim = '';   
                	        $firimend = '';    
							$firimbeg = ''; 	
                	        $k=0;
							$j=0;
                	while (have_rows('gallery')) {
           					the_row();
							if($k==0) $firimend = '<div class="slide" style="background-image: url('. get_sub_field('gallery_img') . ')"></div>';
							$firim .= '<div class="slide" style="background-image: url('. get_sub_field('gallery_img') . ')"></div>';
							$k++;
			
        			}
        			
        			while (have_rows('gallery')) {
           					the_row();
							$j++;
							if($k==$j) $firimbeg = '<div class="slide" style="background-image: url('. get_sub_field('gallery_img') . ')"></div>';
							
							
			
        			}
					echo $firimbeg;
					echo $firim;
					echo $firimend;
					?>                 
                </div>
              </div>
              <div class="gallery-navigation">
                <span class="arr-left"></span>
                <span class="slidecount">
              <span class="slide-current">1</span><span>&nbsp;/&nbsp;</span><span class="slide-total"></span>
                </span>
                <span class="arr-right"></span>
              </div>
            </div>
          </div>
        </div>
        <?php 
        
        $acf_contacts_txt = get_sub_field('contacts_txt');
		$acf_turn_txt = get_sub_field('turn_txt');
        
        
        	} 
}

wp_reset_query();
        
        
        ?>
        <div id="contacts" class="section">
          <div class="column">
            <h2><?php _e('KONTAKTAI', 'theme_site_domainname'); ?></h2>
            <div class="column-text if-mob">
              <p><?php echo $acf_contacts_txt; ?></p>
            </div>
            <form id="contactform" name="contactform">
              <input type="text" id="name" name="name" placeholder="<?php _e('Vardas', 'theme_site_domainname'); ?>">
              <input type="text" id="email" name="email" placeholder="<?php _e('Telefonas arba el. paštas', 'theme_site_domainname'); ?>">
              <textarea id="message" name="message" placeholder="<?php _e('Pranešimas', 'theme_site_domainname'); ?>"></textarea>
              <input type="button" id="submit" class="button" value="<?php _e('Siųsti', 'theme_site_domainname'); ?>">
            </form>
        <?php 
			
			
			$args = array(
	            'post_status' => 'publish',
	            'post_type' => 'contacts',
			'posts_per_page' => -1,
	            'meta_key' => 'default_sort_priority_c',
	            'orderby' => 'meta_value_num',
	            'order' => 'DESC'
	        );	
			
			$brokermob = '';
						
			$the_query = new WP_Query($args);
			if ($the_query->have_posts()) {
				while ($the_query->have_posts()) {
					$the_query->the_post();      
            		$brok_name = get_field('name');
					$brok_phone = get_field('phone');
					$brok_email = get_field('email');
					$brok_image = get_field('image');
            		
            		$brokermob .= '<div class="broker"><img src="';
            		$brokermob .= $brok_image;
					$brokermob .= '" alt="brokername" /><h5 class="brokername">';
					$brokermob .= $brok_name;
					$brokermob .= '</h5><span class="brokerinfo">';
					$brokermob .= $brok_phone;
					$brokermob .= '<br />';
					$brokermob .= $brok_email;
            		$brokermob .= '</span></div>';
					
					
					
					
					
				}
			}
			
			
			
			wp_reset_query();
			
			
			?>
			
			
            
            <div class="brokers if-mob">            
              
              <?php echo $brokermob; ?>
              <!-- <div class="broker">
                <img src="<?php echo get_template_directory_uri(); ?>/img/broker.png" alt="brokername" />
                <h5 class="brokername">Vaida Remeikienė</h5>
                <span class="brokerinfo">
+370 659 37 360<br />
vaida.remeikiene@capital.lt            
            </span>
              </div>  -->
              
                      
           
            </div></div>
          <div class="right">
          	
          	
          	
            <div class="brokers if-desk">
              <h3><?php _e('BROKERIAI', 'theme_site_domainname'); ?></h3>
              
              
              <?php echo $brokermob; ?>
                      
              
              
              
              
            </div>





          </div>
        </div>
      </div>
    </div>
  </div>
    <div id="flats" class="overlay">
    <div class="filtering">
      <div class="filter-back"><?php _e('Atgal', 'theme_site_domainname'); ?></div>
      <div class="filter-block area">
        <h4>PLOTAS</h4>
        <div class="filter filter-from">
          <div class="filter-label"><?php _e('Nuo', 'theme_site_domainname'); ?></div>
          <div class="filter-actions">
            <div class="filter-less"></div>
            <div class="filter-value">
              <span class="selected">35</span>
              <span>40</span>
              <span>45</span>
              <span>50</span>
              <span>55</span>
              <span>60</span>
              <span>65</span>
              <span>70</span> &nbsp;m&sup2;
            </div>
            <div class="filter-more"></div>
          </div>
        </div>
        <div class="filter filter-to">
          <div class="filter-label"><?php _e('Iki', 'theme_site_domainname'); ?></div>
          <div class="filter-actions">
            <div class="filter-less"></div>
            <div class="filter-value">
              <span>35</span>
              <span>40</span>
              <span>45</span>
              <span>50</span>
              <span>55</span>
              <span>60</span>
              <span>65</span>
              <span class="selected">70</span> &nbsp;m&sup2;
            </div>
            <div class="filter-more"></div>
          </div>
        </div>
      </div>
      <div class="filter-block rooms">
        <h4><?php _e('KAMBARIAI', 'theme_site_domainname'); ?></h4>
        <div class="filter filter-from">
          <div class="filter-label"><?php _e('Nuo', 'theme_site_domainname'); ?></div>
          <div class="filter-actions">
            <div class="filter-less"></div>
            <div class="filter-value">
              <span class="selected">2</span>
              <span>3</span>
              <span>4</span>
            </div>
            <div class="filter-more"></div>
          </div>
        </div>
        <div class="filter filter-to">
          <div class="filter-label"><?php _e('Iki', 'theme_site_domainname'); ?></div>
          <div class="filter-actions">
            <div class="filter-less"></div>
            <div class="filter-value">
              <span>2</span>
              <span>3</span>
              <span class="selected">4</span>
            </div>
            <div class="filter-more"></div>
          </div>
        </div>
      </div>
      <div class="filter-block append">
        <div class="button"><?php _e('Išsaugoti', 'theme_site_domainname'); ?></div>
      </div>
    </div>
    <h3>BUTAI</h3>
    <span class="overlay-close"></span>
    <div class="flats">
      <p><?php _e('Žemiau pateikiami laisvų butų duomenys.', 'theme_site_domainname'); ?></p>
      <div class="floor-select">
        <span><?php _e('Namas', 'theme_site_domainname'); ?></span>
        <span id="apt1" class="flatid">1</span>
        <span id="apt2" class="flatid">2</span>
        <span id="apt3" class="flatid">3</span>
      </div>
      <div class="button"><span><?php _e('Filtruoti', 'theme_site_domainname'); ?></span>
      </div>

      <!-- new flat table -->
      <div class="flat-table">
        <div class="row head-row">
          <div class="floor"><span class="foor-cont"><?php _e('Aukštas', 'theme_site_domainname'); ?></span>
          </div>
          <div class="rooms"><span class="rooms-cont"<?php _e('Kambarių skaičius', 'theme_site_domainname'); ?>></span>
          </div>
          <div class="space"><span class="space-cont"><?php _e('Plotas', 'theme_site_domainname'); ?></span>
          </div>
          <div class="price"><span class="price-cont"><?php _e('Kaina', 'theme_site_domainname'); ?></span>
          </div>
        </div>
        
        <?php 
          $position_house = 1;
		  
		  
          	wp_reset_query();
					
					$ap_array = array();
			$args = array(
	            'post_status' => 'publish',
	            'post_type' => 'apartments',          
			'posts_per_page' => -1
	            ,
	            'meta_key' => 'default_sort_priority',
	            'orderby' => 'meta_value_num',
	            'order' => 'DESC',
				 'meta_query' => array(
			        array(
			            'key' => 'location_house',
			            'value' => $position_house,
			            'compare' => '='
			        ),
			        array(
			            'key' => 'status',
			            'value' => 'sold',
			            'compare' => '!='
			        )
			    )
	        );
			
	
	$the_query = new WP_Query($args);
	if ($the_query->have_posts()) {
		while ($the_query->have_posts()) {
			$the_query->the_post();  
				$ap_position_house = get_field('location_house');				
				$ap_position_floor = get_field('location_floor');		
				$ap_url = get_the_permalink();
				$ap_status = get_field('status');								
				$ap_rooms = get_field('rooms');								
				$ap_id = get_field('apartment_id');				
				$ap_area = get_field('area'); 									
				$ap_price = get_field('price');			
				
				
				$ap_orient = get_field('orientation'); 		
				$ap_balconies = get_field('balconies'); 	
				$ap_area_bedroom = get_field('area_bedroom'); 	
				$ap_area_bathroom = get_field('area_bathroom'); 	
				$ap_area_balcony = get_field('area_balcony'); 	
				$ap_area_corridor = get_field('area_corridor2'); 				
				$ap_area_living = get_field('area_living'); 	
				$ap_3d = get_field('image_3d'); 
                $ap_2d = get_field('image_2d'); 					
			?>
        
        <!-- <div class="row flat-row free apt-1A1-1"> -->
        <div class="row flat-row <?php echo $ap_status;?>"> 
          <div class="flat-span">
            <div class="floor"><span class="foor-cont"><?php echo $ap_position_floor;?></span>
            </div>
            <div class="rooms"><span class="rooms-cont"><?php echo $ap_rooms;?></span>
            </div>
            <div class="space"><span class="space-cont"><?php echo $ap_area;?></span>&nbsp;m&sup2;</div>
            <div class="price"><span class="price-cont"><?php echo $ap_price;?></span>&nbsp;&euro;</div>
            <div class="apt-toggle">
              <div class="apt-more"></div>
            </div>
          </div>
          <div class="flat-content">
            <p><?php _e('Būsena:', 'theme_site_domainname'); ?>&nbsp;<span class="apt-status"><?php
              
              if($ap_status == 'reserved') {
              		
              	 _e('Rezervuotas', 'theme_site_domainname');               
			  } elseif ($ap_status == 'free'){
              	
              	 _e('Laisvas', 'theme_site_domainname'); 
              } elseif ($ap_status == 'sold'){
              	 _e('Parduotas', 'theme_site_domainname'); 
			
			  }
              	?></span> </p>
            <p><?php _e('Buto numeris:', 'theme_site_domainname'); ?>&nbsp;<span class="apt-number"><?php echo $ap_id;?></span> </p>
            <p><?php _e('Kambarių skaičius:', 'theme_site_domainname'); ?>&nbsp;<span class="apt-rooms"><?php echo $ap_rooms;?></span> </p>
            <p><?php _e('Kryptis:', 'theme_site_domainname'); ?>&nbsp;<span class="apt-dir"><?php echo $ap_orient;?></span> </p>
            <p><?php _e('Balkonų skaičius:', 'theme_site_domainname'); ?>&nbsp;<span class="apt-balc"><?php echo $ap_balconies;?></span> </p>
            <p><?php _e('Svetainės plotas:', 'theme_site_domainname'); ?>&nbsp;<span class="apt-guest"><?php echo $ap_area_living;?>&nbsp;m&sup2;</span> </p>
            <p><?php _e('Miegamojo plotas:', 'theme_site_domainname'); ?>&nbsp;<span class="apt-bedroom"><?php echo $ap_area_bedroom;?>&nbsp;m&sup2;</span> </p>
            <p><?php _e('Vonios plotas:', 'theme_site_domainname'); ?>&nbsp;<span class="apt-bath"><?php echo $ap_area_bathroom;?>&nbsp;m&sup2;</span> </p>
            <p><?php _e('Holo plotas:', 'theme_site_domainname'); ?>&nbsp;<span class="apt-hall"><?php echo $ap_area_corridor;?>&nbsp;m&sup2;</span> </p>
            <p><?php _e('Balkonų plotai:', 'theme_site_domainname'); ?>&nbsp;<span class="apt-balc-area"><?php echo $ap_area_balcony;?>&nbsp;m&sup2;</span> </p> <img src="<?php echo $ap_3d;?>" class="flat3d" alt="3d flat" />
            
            <?php 
            //if ($ap_position_floor == ...&& $ap_position_house == ..
            ?>
            <div class="floor-plan-container"> <img src="<?php echo get_template_directory_uri(); ?>/img/plan.png" class="floor-plan id001" alt="floor plan" />
              <div class="flat-marker" style="top: 24%; left: 32%;"> </div>
            </div>
            <?php
            //}            
            ?>
          </div>
        </div>
        			
			<?php	

		}
	}


wp_reset_query();

?>
            
        
        
      </div>

    </div>
  </div>
  <div class="tablet-err">
    <div class="err-in"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="logo">
      <p><?php echo $acf_turn_txt; ?></p>
    </div>
  </div>


        <?php wp_footer(); ?>
    </body>
</html>