<?php
load_theme_textdomain( 'theme_site_domainname', TEMPLATEPATH.'/languages' );

if (!function_exists('site_paging_nav')) :

    function site_paging_nav() {
        global $wp_query, $wp_rewrite;

        // Don't print empty markup if there's only one page.
        if ($wp_query->max_num_pages < 2) {
            return;
        }

        $paged = get_query_var('paged') ? intval(get_query_var('paged')) : 1;
        $pagenum_link = html_entity_decode(get_pagenum_link());
        $query_args = array();
        $url_parts = explode('?', $pagenum_link);

        if (isset($url_parts[1])) {
            wp_parse_str($url_parts[1], $query_args);
        }

        $pagenum_link = remove_query_arg(array_keys($query_args), $pagenum_link);
        $pagenum_link = trailingslashit($pagenum_link) . '%_%';

        $format = $wp_rewrite->using_index_permalinks() && !strpos($pagenum_link, 'index.php') ? 'index.php/' : '';
        $format .= $wp_rewrite->using_permalinks() ? user_trailingslashit($wp_rewrite->pagination_base . '/%#%', 'paged') : '?paged=%#%';

        $links = paginate_links(array(
            'base' => $pagenum_link,
            'format' => $format,
            'total' => $wp_query->max_num_pages,
            'current' => $paged,
            'mid_size' => 1,
            'add_args' => array_map('urlencode', $query_args),
            'prev_text' => __(''),
            'next_text' => __(''),
                ));

        if ($links) :
            ?>
            <nav class="navigation paging-navigation" role="navigation">		
                <div class="pagination loop-pagination">
                    <?php echo $links; ?>
                </div>
            </nav>
            <?php
        endif;
    }

endif;

function site_setup() {
    add_theme_support('post-thumbnails');
   // add_image_size('news-front', 320, 178, true);
}
add_action('after_setup_theme', 'site_setup');

function site_scripts_styles() {
    wp_dequeue_script('jquery');
        wp_deregister_script('jquery');
        //wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js", false, null);
        //wp_register_script('jquery',  get_template_directory_uri() . "/js/jquery-1.11.2.min.js", false, null);
       // wp_enqueue_script('jquery');    
    wp_register_script('plugins', get_template_directory_uri() . '/js/plugins.min.js', '', '7', true);
    wp_enqueue_script('plugins');
	//wp_register_script('frontend', get_template_directory_uri() . '/js/frontend.js', '', '2', true);
   // wp_enqueue_script('frontend');		
    wp_enqueue_style('main-style', get_template_directory_uri() . '/css/style.css');
    wp_enqueue_style('site-style', get_stylesheet_uri());
}
add_action('wp_enqueue_scripts', 'site_scripts_styles');

function my_post_type_config() {
    register_post_type('site_config', array(
        'label' => __('Site config'),
        'singular_label' => __('Site config'),
        '_builtin' => false,
        'public' => false,
        'exclude_from_search' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'hierarchical' => true,
        'rewrite' => array('slug' => 'unavailable_page'),
        'capability_type' => 'page',
        'supports' => array(
            'title',
            'editor',
            'custom-fields')
            )
    );
    register_taxonomy('config_categories', 'site_config', array(
        'hierarchical' => true,
        'label' => 'Config categories',
        'singular_name' => 'Config category',
        "rewrite" => false, "query_var" => true)
    );
}
add_action('init', 'my_post_type_config');
function my_post_type_houses() {
    register_post_type('houses', array(
        'label' => __('Site houses'),
        'singular_label' => __('Site house'),
        '_builtin' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'hierarchical' => true,
        'rewrite' => array('slug' => 'namai'),
        'capability_type' => 'page',
        'supports' => array(
            'title',
            'editor',
            'thumbnail',
            'excerpt',
            'custom-fields',
            'comments')
            )
    );
    register_taxonomy('house_categories', 'houses', array(
        'hierarchical' => true,
        'label' => 'Houses categories',
        'singular_name' => 'Houses category',
        "rewrite" => false, "query_var" => true)
    );
}
add_action('init', 'my_post_type_houses');
function my_post_type_apartments() {
    register_post_type('apartments', array(
        'label' => __('Site apartments'),
        'singular_label' => __('Site apartment'),
        '_builtin' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'hierarchical' => true,
        'rewrite' => array('slug' => 'butai'),
        'capability_type' => 'page',
        'supports' => array(
            'title',
            'editor',
            'thumbnail',
            'excerpt',
            'custom-fields',
            'comments')
            )
    );
    register_taxonomy('apartments_categories', 'apartments', array(
        'hierarchical' => true,
        'label' => 'Apartments categories',
        'singular_name' => 'Apartments category',
        "rewrite" => false, "query_var" => true)
    );
}
add_action('init', 'my_post_type_apartments');
function my_post_type_floors() {
    register_post_type('floors', array(
        'label' => __('Site floors'),
        'singular_label' => __('Site floor'),
        '_builtin' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'hierarchical' => true,
        'rewrite' => array('slug' => 'aukstai'),
        'capability_type' => 'page',
        'supports' => array(
            'title',
            'editor',
            'thumbnail',
            'excerpt',
            'custom-fields',
            'comments')
            )
    );
    register_taxonomy('floors_categories', 'floors', array(
        'hierarchical' => true,
        'label' => 'Floors categories',
        'singular_name' => 'Floors category',
        "rewrite" => false, "query_var" => true)
    );
}
add_action('init', 'my_post_type_floors');
function my_post_type_contacts() {
    register_post_type('contacts', array(
        'label' => __('Site contacts'),
        'singular_label' => __('Site contact'),
        '_builtin' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'hierarchical' => true,
        'rewrite' => array('slug' => 'kontaktai'),
        'capability_type' => 'page',
        'supports' => array(
            'title',
            'editor',
            'thumbnail',
            'custom-fields')
            )
    );
}

add_action('init', 'my_post_type_contacts');

function qtranslate_edit_taxonomies() {
    $args = array(
        'public' => true,
        '_builtin' => false
    );
    $output = 'object'; // or objects
    $operator = 'and'; // 'and' or 'or'

    $taxonomies = get_taxonomies($args, $output, $operator);

    if ($taxonomies) {
        foreach ($taxonomies as $taxonomy) {
            add_action($taxonomy->name . '_add_form', 'qtrans_modifyTermFormFor');
            add_action($taxonomy->name . '_edit_form', 'qtrans_modifyTermFormFor');
        }
    }
}

//add_action('admin_init', 'qtranslate_edit_taxonomies');



function custom_upload_mimes ( $existing_mimes=array() ) {

	// add the file extension to the array

	$existing_mimes['svg'] = 'mime/type';

        // call the modified list of extensions

	return $existing_mimes;

}
add_filter('upload_mimes', 'custom_upload_mimes');

function new_excerpt_more2($more) {
    return '...<a class="read-more" href="' . get_permalink(get_the_ID()) . '">' . translate('Plačiau', 'theme_site_domainname') . '</a>';
}
function new_excerpt_more($more) {
    return '<a class="read-more" href="' . get_permalink(get_the_ID()) . '">...</a>';
}

add_filter('excerpt_more', 'new_excerpt_more');


add_filter('show_admin_bar', '__return_false');

remove_action( 'wp_head', 'wp_generator' ) ;
remove_action( 'wp_head', 'wlwmanifest_link' ) ;
remove_action( 'wp_head', 'rsd_link' ) ;

add_action('init', 'remove_header_info');
function remove_header_info() {
	remove_action('wp_head', 'qtranxf_header');
}


function sendbrokeremail() {
	$error = '';
	$status = 'error';
	//if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['message'])) {
	//	$error = 'All fields are required to enter.';
	//} else {
		//if (!wp_verify_nonce($_POST['_acf_nonce'], $_POST['action'])) {
		//	$error = 'Verification error, try again.';
		//} else {
			$name = filter_var($_POST['brokername'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$email = filter_var($_POST['brokeremailemail'], FILTER_SANITIZE_EMAIL);
			$subject = 'Naujamiesčio Alėja - Užklausa';
			$message = stripslashes($_POST['brokermessagemessage']);
			//$message .= PHP_EOL.PHP_EOL.'IP address: '.$_SERVER['REMOTE_ADDR'];
			$message .= PHP_EOL.'Sender\'s name: '.$name;
			$message .= PHP_EOL.'E-mail address / Phone: '.$email;
			$sendmsg = 'Success.';
			$to = get_option('admin_email'); // If you like change this email address
			// replace "noreply@yourdomain.com" with your real email address
			//$header = 'From: '.get_option('blogname').' <noreply@naujamiescioaleja.lt>'.PHP_EOL;
			$header .= 'Reply-To: '.$email.PHP_EOL;			
			$headers = 'Reply-to: ' . $name . ' <' . $email . '>' . ':';
			if ( wp_mail($to, $subject, $message, $header) ) {
				$status = 'success';
				$error = $sendmsg;
			} else {
				$error = 'Some errors occurred.';
			}
		//}
	//}
 
	$resp = array('status' => $status, 'errmessage' => $error);
	header( "Content-Type: application/json" );
	echo json_encode($resp);
	die();
}
add_action( 'wp_ajax_contactform_action', 'ajax_contactform_action_callback' );
add_action( 'wp_ajax_nopriv_contactform_action', 'ajax_contactform_action_callback' );



