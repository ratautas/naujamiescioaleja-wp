<?php
/**
 * Template Name: Search
 */

get_header();

?>
 <?php     if ( have_posts() ){ while ( have_posts() ) : the_post(); 
   
				$position_house = get_field('s_location_house');	
				$p1a = get_field('spage1');		
				$p2a = get_field('spage2');			
				$p3a = get_field('spage3');		
				$p0a = get_the_permalink();			
				
?>   
 <div class="design">
    <img src="<?php echo get_template_directory_uri(); ?>/img/screens/desktop/09_flat_list.jpg" alt="desktop">
    <!--    <img src="img/screens/desktop/01_homepage.jpg" alt="desktop">-->
    <!--    <img src="img/screens/mobile/01_homepage.jpg" alt="mobile">  -->
  </div>
  <div class="wrapper">
    <div class="bodyscroll">
      <div class="bodyscroller">
    <!--
    <div class="nav">
      <nav>
        <div class="mob-nav transparent">
          <a class="small-logo" href="#landing"></a>
          <div class="lang"><a href="#">RU</a>
          </div>
          <div class="nav-toggle">
            <span class="nav-bar"></span>
            <span class="nav-bar"></span>
            <span class="nav-bar"></span>
          </div>
        </div>
        <div class="sidemenu">
          <div class="menu-close"></div>
          <ul>
            <li><a href="index.html" class="menu-flat">BUTAI</a>
            </li>
            <li><a href="#about">APIE PROJEKTĄ</a>
            </li>
            <li><a href="#location">VIETA MIESTE</a>
            </li>
            <li><a href="#gallery">GALERIJA</a>
            </li>
            <li><a href="#contacts">KONTAKTAI</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
-->
    <div id="flat-list">
      <div class="flat-overlay-row">
        <div class="flat-list-head">
          <!--          <div class="flats-list-close">Uždaryti</div>-->
          <div class="flats-list-close">
<!--            <span>-->
<!--
                  <svg width="15.556" height="15.556" viewBox="0 0 15.556 15.557">
                    <g>
                      <path d="m2.417 1.002l14.142 14.143-1.414 1.414-14.142-14.143 1.414-1.414z"/>
                      <path d="m16.559 2.416l-14.142 14.143-1.414-1.414 14.142-14.143 1.414 1.414z"/>
                    </g>
-->
<!--                    </svg>-->
<!--                  </span>-->

            <?php _e(' Uždaryti', 'theme_site_domainname'); ?>
          </div>
          <h1><?php _e('LAISVŲ BUTŲ DUOMENYS IR UŽIMTUMAS', 'theme_site_domainname'); ?></h1>
        </div>
      </div>
      <div class="flat-overlay-row">
        <div class="flat-list-filters">

<!--
          <div class="flat-list-house-sel">
            <span class="filter-key"><?php /* _e('Namas', 'theme_site_domainname'); */ ?></span>
            <a href="<?php /* /* echo $p1a; */ ?>" class="house-id<?php /* echo $p1a == $p0a ? ' selected' : '' ;*/ ?>">1</a>
            <a href="<?php /* echo $p2a; */ ?>" class="house-id<?php /* echo $p2a == $p0a ? ' selected' : '' ;*/ ?>">2</a>
            <a href="<?php /* echo $p3a; */ ?>" class="house-id<?php /* echo $p3a == $p0a ? ' selected' : '' ;*/ ?>">3</a>
          </div>
-->

          <div class="flat-status filter-group">
            <span class="filter-key"><?php _e('Rodyti', 'theme_site_domainname'); ?></span>
            <span class="filter-value"><span><?php _e('Visus', 'theme_site_domainname'); ?></span></span>
            <div class="filter-dropdown">
              <span class="filter-caret"></span>
              <ul>
                <li><span><?php _e('Visus', 'theme_site_domainname'); ?></span>
                </li>
                <li><span><?php _e('Laisvus', 'theme_site_domainname'); ?></span>
                </li>
                <li><span><?php _e('Rezervuotus', 'theme_site_domainname'); ?></span>
                </li>
              </ul>
            </div>
          </div>
          <div class="flat-price-from filter-group">
            <span class="filter-key"><?php _e('Kaina', 'theme_site_domainname'); ?></span>
            <span class="filter-value"><span><?php _e('Nuo', 'theme_site_domainname'); ?></span></span>
            <div class="filter-dropdown">
              <span class="filter-caret"></span>
              <ul>
                <li>Nuo&nbsp;&euro;&nbsp;<span>60000</span>
                </li>
                <li>Nuo&nbsp;&euro;&nbsp;<span>70000</span>
                </li>
                <li>Nuo&nbsp;&euro;&nbsp;<span>80000</span>
                </li>
                <li>Nuo&nbsp;&euro;&nbsp;<span>90000</span>
                </li>
                <li>Nuo&nbsp;&euro;&nbsp;<span>100000</span>
                </li>
                <li>Nuo&nbsp;&euro;&nbsp;<span>110000</span>
                </li>
                <li>Nuo&nbsp;&euro;&nbsp;<span>120000</span>
                </li>
              </ul>
            </div>
          </div>
          <div class="flat-price-to filter-group">
            <span class="filter-value"><span><?php _e('Iki', 'theme_site_domainname'); ?></span></span>
            <div class="filter-dropdown">
              <span class="filter-caret"></span>
              <ul>
                <li>Iki&nbsp;&euro;&nbsp;<span>60000</span>
                </li>
                <li>Iki&nbsp;&euro;&nbsp;<span>70000</span>
                </li>
                <li>Iki&nbsp;&euro;&nbsp;<span>80000</span>
                </li>
                <li>Iki&nbsp;&euro;&nbsp;<span>90000</span>
                </li>
                <li>Iki&nbsp;&euro;&nbsp;<span>100000</span>
                </li>
                <li>Iki&nbsp;&euro;&nbsp;<span>110000</span>
                </li>
                <li>Iki&nbsp;&euro;&nbsp;<span>120000</span>
                </li>
              </ul>
            </div>
          </div>
          <div class="flat-area filter-group">
            <span class="filter-key"><?php _e('Plotas', 'theme_site_domainname'); ?></span>
            <span class="filter-value"><?php _e('Nuo ', 'theme_site_domainname'); ?><span>30</span>&nbsp;m&sup2;</span>
            <div class="filter-dropdown">
              <span class="filter-caret"></span>
              <ul>
                <li>Nuo&nbsp;<span>35</span>&nbsp;m&sup2;
                </li>
                <li>Nuo&nbsp;<span>40</span>&nbsp;m&sup2;
                </li>
                <li>Nuo&nbsp;<span>45</span>&nbsp;m&sup2;
                </li>
                <li>Nuo&nbsp;<span>50</span>&nbsp;m&sup2;
                </li>
                <li>Nuo&nbsp;<span>55</span>&nbsp;m&sup2;
                </li>
                <li>Nuo&nbsp;<span>60</span>&nbsp;m&sup2;
                </li>
                <li>Nuo&nbsp;<span>65</span>&nbsp;m&sup2;
                </li>
              </ul>
            </div>
          </div>
          <div class="button desk-filter-btn"><?php _e('Kaina su parkavimu', 'theme_site_domainname'); ?></div>
        </div>
      </div>
      <div class="flats">
        <!-- new flat table -->

        <div class="flat-table">
          <div class="row head-row">
            <div class="floor"><span class="foor-cont"><?php _e('Aukštas', 'theme_site_domainname'); ?></span>
            </div>
            <div class="rooms"><span class="rooms-cont"><?php _e('Kambarių skaičius', 'theme_site_domainname'); ?></span>
            </div>
            <div class="space"><span class="space-cont"><?php _e('Plotas', 'theme_site_domainname'); ?></span>
            </div>
            <div class="balc"><span class="balc"><?php _e('Balkonas', 'theme_site_domainname'); ?></span>
            </div>
            <div class="direction"><span class="direction-cont"><?php _e('Langų kryptis', 'theme_site_domainname'); ?></span>
            </div>
            <div class="price"><span class="price-cont"><?php _e('Kaina', 'theme_site_domainname'); ?></span>
            </div>
            <div class="flatid"><span class="flatid"><?php _e('Buto numeris', 'theme_site_domainname'); ?></span>
            </div>
            <div class="status"><span class="status-cont"><?php _e('Būsena', 'theme_site_domainname'); ?></span>
            </div>
          </div>
          <?php 
          
          	wp_reset_query();
					
					$ap_array = array();
			$args = array(
	            'post_status' => 'publish',
	            'post_type' => 'apartments',          
			'posts_per_page' => -1
	            ,
	            'meta_key' => 'default_sort_priority',
	            'orderby' => 'meta_value_num',
	            'order' => 'DESC',
				 'meta_query' => array(
			        array(
			            'key' => 'location_house',
			            'value' => $position_house,
			            'compare' => '='
			        ),
			        array(
			            'key' => 'status',
			            'value' => 'sold',
			            'compare' => '!='
			        )
			    )
	        );
			
	
	$the_query = new WP_Query($args);
	if ($the_query->have_posts()) {
		while ($the_query->have_posts()) {
			$the_query->the_post();  
							
				$ap_position_floor = get_field('location_floor');		
				$ap_url = get_the_permalink();
				$ap_status = get_field('status');								
				$ap_rooms = get_field('rooms');								
				$ap_id = get_field('apartment_id');				
				$ap_area = get_field('area'); 									
				$ap_price = get_field('price');				
				$ap_orient_s = get_field('orientations');	
				$ap_balconies = get_field('balconies'); 					
			?>
			
			<a href="<?php echo $ap_url;?>">
            <!-- <div class="row free flat-row apt-1A1-1"> -->
            <div class="row <?php echo $ap_status;?> flat-row">
              <div class="floor"><span class="foor-cont"><?php echo $ap_position_floor;?></span>
              </div>
              <div class="rooms"><span class="rooms-cont"><?php echo $ap_rooms;?></span>
              </div>
              <div class="space"><span class="space-cont"><?php echo $ap_area;?></span>&nbsp;m&sup2;</div>
              <div class="balc"><span class="balc"><?php echo $ap_balconies;?></span>
              </div>
              <div class="direction"><span class="direction-cont"><?php echo $ap_orient_s;?></span>
              </div>
              <div class="price">&euro;&nbsp;<span class="price-cont"><?php echo $ap_price;?></span>
              </div>
              <div class="flatid"><span class="flatid"<?php echo $ap_id;?></span>
              </div>
              <div class="status"><span class="status-cont"><?php
              
              if($ap_status == 'reserved') {
              		
              	 _e('Rezervuotas', 'theme_site_domainname');               
			  } elseif ($ap_status == 'free'){
              	
              	 _e('Laisvas', 'theme_site_domainname'); 
              } elseif ($ap_status == 'sold'){
              	 _e('Parduotas', 'theme_site_domainname'); 
			
			  }
              	
              	
              	
              	?></span>
              </div>
              <div class="apt-toggle">
                <div class="apt-more"></div>
              </div>
            </div>
          </a>       
			
			
			
			<?php	

		}
	}


wp_reset_query();

?>
             
          <div class="flat-table-error row">
            <?php _e('Nerasta:(', 'theme_site_domainname'); ?>
          </div>
        </div>


      </div>
    </div>
      </div>
    </div>
  </div>
  <?php endwhile;}?>   

 <?php wp_footer(); ?>

</body>

</html>
