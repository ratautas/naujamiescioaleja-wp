<?php
get_header();
?>

<?php     if ( have_posts() ){ while ( have_posts() ) : the_post(); 
   
                $thecontent = get_the_content(); 
				$position_house = get_field('location_house');				
				$position_floor = get_field('location_floor');
				$position_apartment = get_field('location_apartment');
				//$plan_name = get_the_excerpt();
				//$thepermalink = get_the_permalink();
				$ap_pos = get_field('location_apartment');
				$ap_url = get_the_permalink();
				$ap_status = get_field('status');				
				$ap_rooms = get_field('rooms');				
				$ap_id = get_field('apartment_id');
				$ap_area = get_field('area'); 					
				$ap_price = get_field('price');
				$ap_pricef = number_format($ap_price, 0, ',', ' ');
				$ap_title = get_the_title();
				$ap_orient = get_field('orientation'); 
				$ap_orient_s = get_field('orientations'); 		
				$ap_balconies = get_field('balconies'); 	
				$ap_area_bedroom = get_field('area_bedroom'); 	
				$ap_area_bathroom = get_field('area_bathroom'); 	
				$ap_area_balcony = get_field('area_balcony'); 	
				$ap_area_corridor = get_field('area_corridor2'); 				
				$ap_area_living = get_field('area_living'); 	
				$ap_3d = get_field('image_3d'); 
                $ap_2d = get_field('image_2d'); 
?>            


<div class="design">
    <!--    <img src="img/screens/desktop/05_plan.jpg" alt="desktop">-->
        <img src="<?php echo get_template_directory_uri(); ?>/img/screens/desktop/06_flat.jpg" alt="desktop">
    <!--    <img src="img/screens/desktop/08_list.jpg" alt="desktop">-->
<!--    <img src="img/screens/desktop/07_flat_list.jpg" alt="desktop">-->
    <!--    <img src="img/screens/mobile/01_homepage.jpg" alt="mobile">  -->
  </div>
  <div class="wrapper">
    <!--
    <div class="nav">
      <nav>
        <div class="mob-nav transparent">
          <a class="small-logo" href="#landing"></a>
          <div class="lang"><a href="#">RU</a>
          </div>
          <div class="nav-toggle">
            <span class="nav-bar"></span>
            <span class="nav-bar"></span>
            <span class="nav-bar"></span>
          </div>
        </div>
        <div class="sidemenu">
          <div class="menu-close"></div>
          <ul>
            <li><a href="index.html" class="menu-flat">BUTAI</a>
            </li>
            <li><a href="#about">APIE PROJEKTĄ</a>
            </li>
            <li><a href="#location">VIETA MIESTE</a>
            </li>
            <li><a href="#gallery">GALERIJA</a>
            </li>
            <li><a href="#contacts">KONTAKTAI</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
-->
    <div class="bodyscroll">
      <div class="bodyscroller">
        <div id="landing" class="section">
          <div class="house if-desk">
            <div class="column">
              <div class="house-actions">
                <div class="house-overlay-close">
<!--
                  <span>
                  <svg width="15.556" height="15.556" viewBox="0 0 15.556 15.557">
                    <g>
                      <path d="m2.417 1.002l14.142 14.143-1.414 1.414-14.142-14.143 1.414-1.414z"/>
                      <path d="m16.559 2.416l-14.142 14.143-1.414-1.414 14.142-14.143 1.414 1.414z"/>
                    </g>
                    </svg>
                  </span>
-->
                  <?php _e(' Uždaryti', 'theme_site_domainname'); ?>
                </div>
                <?php

                wp_reset_query();

					
						$args = array(
						            'post_status' => 'publish',
						            'post_type' => 'houses',
						            'posts_per_page' => 1,						          
					'meta_query' => array(
					        array(
					            'key' => 'h_location_house',
					            'value' => $position_house,
					            'compare' => '='
					        )
					    )
						);
						$the_query = new WP_Query($args);
						if ($the_query->have_posts()) {
							while ($the_query->have_posts()) {
							$the_query->the_post(); 
								
							
					?>
                <div class="house-navigation">
                  <a href="<?php the_field('house_link_left'); ?>" ><div class="house-prev-arr">
<!--
                    <svg height="14" viewBox="0 0 8 14" width="8">
                      <path d="m6.4824 0.0019531 1.5176 1.4042-5.1816 5.5938 5.1816 5.596-1.5176 1.404-6.4824-7 6.4824-6.998z" />
                    </svg>
-->
                  </div></a>
                  <div class="house-title"><?php the_title(); ?></div>
                  <a href="<?php the_field('house_link_right'); ?>" ><div class="house-next-arr">
<!--
                    <svg height="14" viewBox="0 0 8 14" width="8">
                      <path d="m1.5176 0.0019531-1.5176 1.4042 5.1816 5.5938-5.1816 5.596 1.5176 1.404 6.4824-7-6.4824-6.998z" />
                    </svg>
-->
                  </div></a>
                </div>
              </div>
              <div class="house-text"><?php echo $thecontent; ?></div>
              <div class="house-action-text"><?php _e('Pasirinkite aukštą, kad susipažint su butų išdėstymu.', 'theme_site_domainname'); ?>
              </div>
              <span class="button contact"><?php _e('Susisiekti su vadybininku', 'theme_site_domainname'); ?></span>
            </div>
            <div class="floor-stripe bordered">
              <span class="floor-label"><?php _e('AUKŠTAS', 'theme_site_domainname'); ?></span>
              <?php 
                	$h_location_house = get_field('h_location_house');
                	
          			$floors = array();
                	
                	while (have_rows('floor_menu')) {
           					the_row();
							
							$a = get_sub_field('h_floor_name');
							$b = get_sub_field('h_floor_link');
							$c = get_sub_field('h_floor_overlay_id');
							$floors[$c]['location'] = $a;
							$floors[$c]['link'] = $b;
							$floors[$c]['overlay'] = $c;
			
        			}
					
					
					
			?> 
              <?php
              foreach ($floors as $value) {
    				if ($position_floor == $value['location']) {
				  	echo '<a class="floor-sel active" href="' . $value['link'] . '" id="floorsel-a' . $value['overlay'] . '">' . $value['location'] . '</a>';
					} else {
						
				  	echo '<a class="floor-sel" href="' . $value['link'] . '" id="floorsel-a' . $value['overlay'] . '">' . $value['location'] . '</a>';
						
					}
				}
				
			  ?>
              <span class="sel-border"></span>
            </div>
           
            <div class="floor-title">

<!--              cia turetu eiti realus linkas i auksta:-->
              <a class="floor-title-text" href="#">
              	 <?php 
              	 
              	 
           			}
            	
            	}
										
					wp_reset_query();
					
					

					
						$args = array(
						            'post_status' => 'publish',
						            'post_type' => 'floors',
						            'posts_per_page' => 1,						          
					'meta_query' => array(
					        array(
					            'key' => 'f_location_house',
					            'value' => $position_house,
					            'compare' => '='
					        ),
					        array(
					            'key' => 'f_location_floor',
					            'value' => $position_floor,
					            'compare' => '='
					        )
					    )
						);
						$the_query = new WP_Query($args);
						if ($the_query->have_posts()) {
							while ($the_query->have_posts()) {
							$the_query->the_post(); 
								
					the_excerpt();
							}}
					
					
					
					wp_reset_query();
					
					?>               
              </a>
            </div>
            <div class="shadow"></div>
            <div class="right">
              <div class="flat-page">
                <div class="flat-top">
                  <a class="back-to-floor" href="#"><span class="back-arr"></span><?php _e('Rodyti aukšto planą', 'theme_site_domainname'); ?></a>
                  <h1><?php _e('BUTAS NR.: ', 'theme_site_domainname'); ?><?php echo $ap_title; ?></h1>
                  <div class="flat-price"><?php echo $ap_pricef; ?>&nbsp;&euro;</div>
                  <div class="view-switch twod">
                    <span class="threed-text">3D</span>
                    <span class="twod-text">2D</span>
                    <span class="view-switch-button"></span>
                  </div>
                  <div class="compass pv"></div>
                </div>
                <div class="flat-content">
                  <div class="flat-image">
                    <div class="threed-img" style="background-image: url('<?php echo $ap_3d; ?>')"></div>
                    <div class="twod-img" style="background-image: url('<?php echo $ap_2d; ?>')"></div>
<!--
                    <div class="threed-img" style="background-image: url('../img/flat-3d.jpg')"></div>
                    <div class="twod-img" style="background-image: url('../img/flat-2d-plan.jpg')"></div>
-->
                  </div>
                  <div class="flat-details">
                    <div class="col col1">
                      <p><?php _e('Kambarių skaičius:', 'theme_site_domainname'); ?>&nbsp;<span class="flat-rooms">3</span>
                      </p>
                      <p><?php _e('Plotas:', 'theme_site_domainname'); ?>&nbsp;<span class="flat-space"><?php echo $ap_area; ?>&nbsp;m&sup2;</span>
                      </p>
                      <p><?php _e('Langų kryptis:', 'theme_site_domainname'); ?>&nbsp;<span class="flat-dir"><?php echo $ap_orient; ?></span>
                      </p>
                      <p><?php _e('Balkonų skaičius:', 'theme_site_domainname'); ?>&nbsp;<span class="flat-balc"><?php echo $ap_balconies; ?></span>
                      </p>
                      <p><?php _e('Svetainės plotas:', 'theme_site_domainname'); ?>&nbsp;<span class="flat-guest"><?php echo $ap_area_living; ?>&nbsp;m&sup2;</span>
                      </p>
                    </div>
                    <div class="col col2">
                      <p><?php _e('Miegamojo plotas:', 'theme_site_domainname'); ?>&nbsp;<span class="flat-bedroom"><?php echo $ap_area_bedroom; ?>&nbsp;m&sup2;</span>
                      </p>
                      <p><?php _e('Vonios plotas:', 'theme_site_domainname'); ?>&nbsp;<span class="flat-bath"><?php echo $ap_area_bathroom; ?>&nbsp;m&sup2;</span>
                      </p>
                      <p><?php _e('Holas:', 'theme_site_domainname'); ?>&nbsp;<span class="flat-hall"><?php echo $ap_area_corridor; ?>&nbsp;m&sup2;</span>
                      </p>                      
                      <p><?php _e('Balkonų plotai:', 'theme_site_domainname'); ?>&nbsp;<span class="flat-balc-area"><?php echo $ap_area_balcony; ?>&nbsp;m&sup2;</span>
                      </p>
                    </div>
                    <div class="col col3">
                      <div class="button similar-btn"><?php _e('Kiti panašūs butai', 'theme_site_domainname'); ?></div>
                      <div class="button contact-broker-btn"><?php _e('Susisiekti su brokeriu', 'theme_site_domainname'); ?></div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="slide-overlay">
            <div class="slide-side">
              <div class="close-side">                
<!--
                  <svg width="15.556" height="15.556" viewBox="0 0 15.556 15.557">
                    <g>
                      <path d="m2.417 1.002l14.142 14.143-1.414 1.414-14.142-14.143 1.414-1.414z"/>
                      <path d="m16.559 2.416l-14.142 14.143-1.414-1.414 14.142-14.143 1.414 1.414z"/>
                    </g>
                    </svg>                
-->
              </div>
              <?php 
              
              $post_object = get_field('related_contact');
				
				if( $post_object ): 

					$post = $post_object;
					setup_postdata( $post ); 
					
					
					?>
              <div class="contact-broker">
                <h3><?php _e('Susisiekite', 'theme_site_domainname'); ?></h3>
                <div class="broker">
                  <img src="<?php the_field('image'); ?>" alt="<?php the_field('name'); ?>" />
                  <h5 class="brokername"><?php the_field('name'); ?></h5>
                  <span class="brokerinfo">
<?php the_field('phone'); ?><br />
<?php the_field('email'); ?>           
            </span>
                </div>
                <span id="contact-msg"></span>
                <form id="brokerform" name="brokerform">
                  <input type="text" id="brokername" name="brokername" placeholder="<?php _e('Vardas', 'theme_site_domainname'); ?>">
                  <input type="hidden" id="brokerid" name="brokerid" value="<?php the_ID(); ?>">
                  <input type="text" id="brokeremail" name="brokeremailemail" placeholder="<?php _e('Telefonas arba el. paštas', 'theme_site_domainname'); ?>">
                  <textarea id="brokermessage" name="brokermessagemessage" placeholder="<?php _e('Pranešimas', 'theme_site_domainname'); ?>"></textarea>
                  <input type="button" id="brokersubmit" class="button" value="<?php _e('Siųsti', 'theme_site_domainname'); ?>">          
                </form>
              </div>
              <script>
              	
        function formsubmit() {
            $('#brokersubmit').click(function() {				
				
								$.ajax({
									type: 'POST',
									url: ajaxurl + '/sendbrokeremail',
									data: $('#brokerform').serialize(),
									dataType: 'json',
									success: function(response) {
										if (response.status == 'success') {
											
											$('#contact-msg').html(response.errmessage);
											$('#brokerform')[0].reset();
										}
										$('#contact-msg').html(response.errmessage);
									}
								});
										
			});
            
            
        }
        window.onload = formsubmit;
              	
              </script>
            
<?php 
		wp_reset_postdata();
		endif;
  ?>
              <div class="similar">
                <h3><?php _e('Panašūs butai', 'theme_site_domainname'); ?></h3>
                <!-- <div class="flat-table">
                  <div class="row head-row">
                    <div class="rooms"><span class="rooms-cont">Kambariai</span>
                    </div>
                    <div class="floor"><span class="foor-cont">Aukštas</span>
                    </div>
                    <div class="price"><span class="price-cont">Kaina</span>
                    </div>
                    <div class="apt-toggle">
                      <div class="apt-more"></div>
                    </div>
                  </div>
                  <a href="flat.html">
                    <div class="row flat-row apt-1A1-1">
                      <div class="rooms"><span class="rooms-cont">3</span>
                      </div>
                      <div class="floor"><span class="foor-cont">1</span>
                      </div>
                      <div class="price"><span class="price-cont">82091</span>&euro;</div>
                      <div class="apt-toggle">
                        <div class="apt-more"></div>
                      </div>
                    </div>
                  </a>
                  <a href="flat.html">
                    <div class="row flat-row apt-1A1-1">
                      <div class="rooms"><span class="rooms-cont">3</span>
                      </div>
                      <div class="floor"><span class="foor-cont">1</span>
                      </div>
                      <div class="price"><span class="price-cont">82091</span>&euro;</div>
                      <div class="apt-toggle">
                        <div class="apt-more"></div>
                      </div>
                    </div>
                  </a>
                  <a href="flat.html">
                    <div class="row flat-row apt-1A1-1">
                      <div class="rooms"><span class="rooms-cont">3</span>
                      </div>
                      <div class="floor"><span class="foor-cont">1</span>
                      </div>
                      <div class="price"><span class="price-cont">82091</span>&euro;</div>
                      <div class="apt-toggle">
                        <div class="apt-more"></div>
                      </div>
                    </div>
                  </a>
                  <a href="flat.html">
                    <div class="row flat-row apt-1A1-1">
                      <div class="rooms"><span class="rooms-cont">3</span>
                      </div>
                      <div class="floor"><span class="foor-cont">1</span>
                      </div>
                      <div class="price"><span class="price-cont">82091</span>&euro;</div>
                      <div class="apt-toggle">
                        <div class="apt-more"></div>
                      </div>
                    </div>
                  </a>
                  <a href="flat.html">
                    <div class="row flat-row apt-1A1-1">
                      <div class="rooms"><span class="rooms-cont">3</span>
                      </div>
                      <div class="floor"><span class="foor-cont">1</span>
                      </div>
                      <div class="price"><span class="price-cont">82091</span>&euro;</div>
                      <div class="apt-toggle">
                        <div class="apt-more"></div>
                      </div>
                    </div>
                  </a>
                </div> -->


              </div>
            </div>
          </div>
        </div>









<?php endwhile;}?>   

<?php get_footer(); ?>
