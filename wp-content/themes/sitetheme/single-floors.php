<?php
get_header();
?>
<?php     if ( have_posts() ){ while ( have_posts() ) : the_post();
   
                $thecontent = get_the_content(); 
				$position_house = get_field('f_location_house');				
				$position_floor = get_field('f_location_floor');
				$plan_name = get_the_excerpt();
				$thepermalink = get_the_permalink();
                
?> 
         




  <div class="design">
    <img src="<?php echo get_template_directory_uri(); ?>/img/screens/desktop/05_plan.jpg" alt="desktop">
    <!--    <img src="img/screens/mobile/01_homepage.jpg" alt="mobile">  -->
  </div>
  <div class="wrapper">
    <!--
    <div class="nav">
      <nav>
        <div class="mob-nav transparent">
          <a class="small-logo" href="#landing"></a>
          <div class="lang"><a href="#">RU</a>
          </div>
          <div class="nav-toggle">
            <span class="nav-bar"></span>
            <span class="nav-bar"></span>
            <span class="nav-bar"></span>
          </div>
        </div>
        <div class="sidemenu">
          <div class="menu-close"></div>
          <ul>
            <li><a href="index.html" class="menu-flat">BUTAI</a>
            </li>
            <li><a href="#about">APIE PROJEKTĄ</a>
            </li>
            <li><a href="#location">VIETA MIESTE</a>
            </li>
            <li><a href="#gallery">GALERIJA</a>
            </li>
            <li><a href="#contacts">KONTAKTAI</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
-->
    <div class="bodyscroll">
      <div class="bodyscroller">
        <div id="landing" class="section">
          <div class="house if-desk">
            <div class="column">
              <div class="house-actions">
                <div class="house-overlay-close">
<!--
                  <span>
                  <svg width="15.556" height="15.556" viewBox="0 0 15.556 15.557">
                    <g>
                      <path d="m2.417 1.002l14.142 14.143-1.414 1.414-14.142-14.143 1.414-1.414z"/>
                      <path d="m16.559 2.416l-14.142 14.143-1.414-1.414 14.142-14.143 1.414 1.414z"/>
                    </g>
                    </svg>
                  </span>
-->
                  <?php _e(' Uždaryti', 'theme_site_domainname'); ?>
                </div>
                <?php

                wp_reset_query();

					
						$args = array(
						            'post_status' => 'publish',
						            'post_type' => 'houses',
						            'posts_per_page' => 1,						          
					'meta_query' => array(
					        array(
					            'key' => 'h_location_house',
					            'value' => $position_house,
					            'compare' => '='
					        )
					    )
						);
						$the_query = new WP_Query($args);
						if ($the_query->have_posts()) {
							while ($the_query->have_posts()) {
							$the_query->the_post(); 
								
							
					?>
                <div class="house-navigation">
                  <a href="<?php the_field('house_link_left'); ?>" ><div class="house-prev-arr">
<!--
                    <svg height="14" viewBox="0 0 8 14" width="8">
                      <path d="m6.4824 0.0019531 1.5176 1.4042-5.1816 5.5938 5.1816 5.596-1.5176 1.404-6.4824-7 6.4824-6.998z" />
                    </svg>
-->
                    </a>
                  </div>
                  <div class="house-title"><?php the_title(); ?></div>
                  <a href="<?php the_field('house_link_right'); ?>" ><div class="house-next-arr">
<!--
                    <svg height="14" viewBox="0 0 8 14" width="8">
                      <path d="m1.5176 0.0019531-1.5176 1.4042 5.1816 5.5938-5.1816 5.596 1.5176 1.404 6.4824-7-6.4824-6.998z" />
                    </svg>
-->
                  </div></a>
                </div>
              </div>
              <div class="house-text"><?php echo $thecontent; ?></div>
              <div class="house-action-text"><?php _e('Pasirinkite aukštą, kad susipažint su butų išdėstymu.', 'theme_site_domainname'); ?>
              </div>
              <span class="button contact"><?php _e('Susisiekti su vadybininku', 'theme_site_domainname'); ?></span>
            </div>
            <div class="floor-stripe bordered">
              <span class="floor-label"><?php _e('AUKŠTAS', 'theme_site_domainname'); ?></span>
              <?php 
                	$h_location_house = get_field('h_location_house');
                	
          			$floors = array();
                	
                	while (have_rows('floor_menu')) {
           					the_row();
							
							$a = get_sub_field('h_floor_name');
							$b = get_sub_field('h_floor_link');
							$c = get_sub_field('h_floor_overlay_id');
							$floors[$c]['location'] = $a;
							$floors[$c]['link'] = $b;
							$floors[$c]['overlay'] = $c;
			
        			}
					
					
					
			?> 
              <?php
              foreach ($floors as $value) {
    				if ($thepermalink == $value['link']) {
				  	echo '<a class="floor-sel active" href="' . $value['link'] . '" id="floorsel-a' . $value['overlay'] . '">' . $value['location'] . '</a>';
					} else {
						
				  	echo '<a class="floor-sel" href="' . $value['link'] . '" id="floorsel-a' . $value['overlay'] . '">' . $value['location'] . '</a>';
						
					}
				}
				
			  ?>
              <span class="sel-border"></span>
            </div>
            <?php 
           			}
            	
            	}
										
					wp_reset_query();
					
					$ap_array = array();
$args = array(
	            'post_status' => 'publish',
	            'post_type' => 'apartments',          
			'posts_per_page' => -1
	            ,
				 'meta_query' => array(
			        array(
			            'key' => 'location_house',
			            'value' => $position_house,
			            'compare' => '='
			        ),
			        array(
			            'key' => 'location_floor',
			            'value' => $position_floor,
			            'compare' => '='
			        )
			    )
	        );
	
	$the_query = new WP_Query($args);
	if ($the_query->have_posts()) {
		while ($the_query->have_posts()) {
			$the_query->the_post();  
						
			$ap_pos = get_field('location_apartment');
			$ap_url = get_the_permalink();
			$ap_status = get_field('status');
			//$ap_status = $ap_status == 'booked' ? 'reserved' : $ap_status;
			$ap_rooms = get_field('rooms');				
			$ap_id = get_field('apartment_id');
			$ap_area = get_field('area'); 					
			$ap_price = get_field('price'); 	
			$ap_array[$ap_pos]['url'] = $ap_url;
			$ap_array[$ap_pos]['status'] = $ap_status;
			$ap_array[$ap_pos]['rooms'] = $ap_rooms;
			$ap_array[$ap_pos]['id'] = $ap_id;
			$ap_array[$ap_pos]['area'] = $ap_area;
			$ap_array[$ap_pos]['price'] = $ap_price;
			$ap_array[$ap_pos]['pos'] = $ap_pos;

		}
	}


wp_reset_query();
					
                
                
                
                ?>
            <div class="right">
              <div class="floor-page">
                <div class="top-text">
                  <h3><?php echo $plan_name ?></h3>
                  <span class="floor-subtitle"><?php _e('Išisirinkite butą', 'theme_site_domainname'); ?></span>
                  <div class="compass pv"></div>
                </div>                
                	<?php 
                	switch ($position_floor) {						    
						    	case 1:
						        ?>
								<div class="floor-plan" id="floor-a1">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="825" height="600" viewBox="0 0 825 600">
                    <defs>
                      <pattern id="free" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a1.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="hover" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a1-hover.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="reserved" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a1-reserved.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="sold" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a1-sold.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                    </defs>
                    <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a1.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                    <path d="M783.107,196.589 C783.107,196.589 565.789,196.589 565.789,196.589 C565.789,196.589 565.789,264.895 565.789,264.895 C565.789,264.895 490.335,264.895 490.335,264.895 C490.335,264.895 490.335,196.589 490.335,196.589 C490.335,196.589 476.529,196.589 476.529,196.589 C476.529,196.589 476.529,41.650 476.529,41.650 C476.529,41.650 531.751,41.650 531.751,41.650 C531.751,41.650 531.751,0.000 531.751,0.000 C531.751,0.000 650.289,0.000 650.289,0.000 C650.289,0.000 650.289,41.650 650.289,41.650 C650.289,41.650 783.107,41.650 783.107,41.650 C783.107,41.650 783.107,196.589 783.107,196.589 Z" id="flat-a1-1" data-url="<?php echo $ap_array['1']['url']; ?>" class="<?php echo $ap_array['1']['status']; ?> flat-shape" fill-rule="evenodd" />
                    <path d="M824.762,330.107 C824.762,330.107 783.107,330.107 783.107,330.107 C783.107,330.107 783.107,396.271 783.107,396.271 C783.107,396.271 564.599,396.271 564.599,396.271 C564.599,396.271 564.599,197.303 564.599,197.303 C564.599,197.303 783.107,197.303 783.107,197.303 C783.107,197.303 783.107,211.583 783.107,211.583 C783.107,211.583 824.762,211.583 824.762,211.583 C824.762,211.583 824.762,330.107 824.762,330.107 Z" id="flat-a1-2" data-url="<?php echo $ap_array['2']['url']; ?>" class="<?php echo $ap_array['2']['status']; ?> flat-shape" fill-rule="evenodd" />
                    <path d="M783.107,558.112 C783.107,558.112 639.101,558.112 639.101,558.112 C639.101,558.112 639.101,599.762 639.101,599.762 C639.101,599.762 526.515,599.762 526.515,599.762 C526.515,599.762 526.515,558.112 526.515,558.112 C526.515,558.112 479.147,558.112 479.147,558.112 C479.147,558.112 479.147,329.869 479.147,329.869 C479.147,329.869 565.551,329.869 565.551,329.869 C565.551,329.869 565.551,402.221 565.551,402.221 C565.551,402.221 783.107,402.221 783.107,402.221 C783.107,402.221 783.107,558.112 783.107,558.112 Z" id="flat-a1-3" data-url="<?php echo $ap_array['3']['url']; ?>" class="<?php echo $ap_array['3']['status']; ?> flat-shape" fill-rule="evenodd" />
                    <path d="M468.912,558.112 C468.912,558.112 468.912,599.524 468.912,599.524 C468.912,599.524 350.375,599.524 350.375,599.524 C350.375,599.524 350.375,558.112 350.375,558.112 C350.375,558.112 290.154,558.112 290.154,558.112 C290.154,558.112 290.154,329.869 290.154,329.869 C290.154,329.869 481.052,329.869 481.052,329.869 C481.052,329.869 481.052,558.112 481.052,558.112 C481.052,558.112 468.912,558.112 468.912,558.112 Z" id="flat-a1-4" data-url="<?php echo $ap_array['4']['url']; ?>" class="<?php echo $ap_array['4']['status']; ?> flat-shape" fill-rule="evenodd" />
                    <path d="M183.756,558.826 C183.756,558.826 183.756,599.762 183.756,599.762 C183.756,599.762 65.457,599.762 65.457,599.762 C65.457,599.762 65.457,558.826 65.457,558.826 C65.457,558.826 41.893,558.826 41.893,558.826 C41.893,558.826 41.893,329.869 41.893,329.869 C41.893,329.869 288.250,329.869 288.250,329.869 C288.250,329.869 288.250,558.826 288.250,558.826 C288.250,558.826 183.756,558.826 183.756,558.826 Z" id="flat-a1-5" data-url="<?php echo $ap_array['5']['url']; ?>" class="<?php echo $ap_array['5']['status']; ?> flat-shape" fill-rule="evenodd" />
                    <path d="M243.263,270.369 C243.263,270.369 243.263,333.915 243.263,333.915 C243.263,333.915 139.484,333.915 139.484,333.915 C139.484,333.915 139.484,339.389 139.484,339.389 C139.484,339.389 41.893,339.389 41.893,339.389 C41.893,339.389 41.893,243.237 41.893,243.237 C41.893,243.237 -0.000,243.237 -0.000,243.237 C-0.000,243.237 -0.000,126.616 -0.000,126.616 C-0.000,126.616 41.893,126.616 41.893,126.616 C41.893,126.616 41.893,41.888 41.893,41.888 C41.893,41.888 139.484,41.888 139.484,41.888 C139.484,41.888 139.484,194.209 139.484,194.209 C139.484,194.209 270.636,194.209 270.636,194.209 C270.636,194.209 270.636,270.369 270.636,270.369 C270.636,270.369 243.263,270.369 243.263,270.369 Z" id="flat-a1-6" data-url="<?php echo $ap_array['6']['url']; ?>" class="<?php echo $ap_array['6']['status']; ?> flat-shape" fill-rule="evenodd" />
                    <path d="M350.137,197.303 C350.137,197.303 350.137,267.037 350.137,267.037 C350.137,267.037 266.352,267.037 266.352,267.037 C266.352,267.037 266.352,197.303 266.352,197.303 C266.352,197.303 138.055,197.303 138.055,197.303 C138.055,197.303 138.055,41.888 138.055,41.888 C138.055,41.888 194.706,41.888 194.706,41.888 C194.706,41.888 194.706,-0.000 194.706,-0.000 C194.706,-0.000 330.143,-0.000 330.143,-0.000 C330.143,-0.000 330.143,41.888 330.143,41.888 C330.143,41.888 354.183,41.888 354.183,41.888 C354.183,41.888 354.183,197.303 354.183,197.303 C354.183,197.303 350.137,197.303 350.137,197.303 Z" id="flat-a1-7" data-url="<?php echo $ap_array['7']['url']; ?>" class="<?php echo $ap_array['7']['status']; ?> flat-shape" fill-rule="evenodd" />
                  </svg>
                   <?php
              foreach ($ap_array as $value) { ?>
    							
    					<div class="flat-mark" id="mark-a1-<?php echo $value['pos'] ?>" data-url="<?php echo $value['url'] ?>">                  
                    <div class="floor-circle">
                      <div class="circle">
                        <div class="circle-in"><?php echo $value['id'] ?></div>
                      </div>
                      <div class="floor-pop-snap">
                        <div class="floor-pop free">
                          <p><?php _e('Kaina: ', 'theme_site_domainname'); ?><?php echo number_format($value['price'], 0, ',', ' '); ?> €</p>
                          <p><?php _e('Plotas: ', 'theme_site_domainname'); ?><?php echo $value['area'] ?> m<sup>2</sup></p>
                          <p><?php _e('Kambarių skaičus: ', 'theme_site_domainname'); ?><?php echo $value['rooms'] ?></p>
                        </div>
                      </div>
                    </div>                  
                  </div>
				<?php }
				
			  ?></div>
			  <?php
						        break;
						    	case 2:?>
								<div class="floor-plan" id="floor-a2">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="825" height="600" viewBox="0 0 825 600">
                    <defs>
                      <pattern id="free" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a2.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="hover" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a2-hover.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="reserved" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a2-reserved.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="sold" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a2-sold.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                    </defs>
                    <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a2.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                    <path d="M783.107,196.589 C783.107,196.589 565.789,196.589 565.789,196.589 C565.789,196.589 565.789,264.895 565.789,264.895 C565.789,264.895 490.335,264.895 490.335,264.895 C490.335,264.895 490.335,196.589 490.335,196.589 C490.335,196.589 476.529,196.589 476.529,196.589 C476.529,196.589 476.529,41.650 476.529,41.650 C476.529,41.650 531.751,41.650 531.751,41.650 C531.751,41.650 531.751,0.000 531.751,0.000 C531.751,0.000 650.288,0.000 650.288,0.000 C650.288,0.000 650.288,41.650 650.288,41.650 C650.288,41.650 783.107,41.650 783.107,41.650 C783.107,41.650 783.107,196.589 783.107,196.589 Z" id="flat-a2-1" data-url="<?php echo $ap_array['1']['url']; ?>" class="<?php echo $ap_array['1']['status']; ?> fill-rule="evenodd" />
                    <path d="M824.762,330.107 C824.762,330.107 783.107,330.107 783.107,330.107 C783.107,330.107 783.107,396.271 783.107,396.271 C783.107,396.271 564.361,396.271 564.361,396.271 C564.361,396.271 564.361,197.303 564.361,197.303 C564.361,197.303 783.107,197.303 783.107,197.303 C783.107,197.303 783.107,211.583 783.107,211.583 C783.107,211.583 824.762,211.583 824.762,211.583 C824.762,211.583 824.762,330.107 824.762,330.107 Z" id="flat-a2-2" data-url="<?php echo $ap_array['2']['url']; ?>" class="<?php echo $ap_array['2']['status']; ?> fill-rule="evenodd" />
                    <path d="M783.107,558.112 C783.107,558.112 639.101,558.112 639.101,558.112 C639.101,558.112 639.101,599.762 639.101,599.762 C639.101,599.762 526.515,599.762 526.515,599.762 C526.515,599.762 526.515,558.112 526.515,558.112 C526.515,558.112 479.147,558.112 479.147,558.112 C479.147,558.112 479.147,329.869 479.147,329.869 C479.147,329.869 565.551,329.869 565.551,329.869 C565.551,329.869 565.551,402.221 565.551,402.221 C565.551,402.221 783.107,402.221 783.107,402.221 C783.107,402.221 783.107,558.112 783.107,558.112 Z" id="flat-a2-3" data-url="<?php echo $ap_array['3']['url']; ?>" class="<?php echo $ap_array['3']['status']; ?> fill-rule="evenodd" />
                    <path d="M468.912,558.112 C468.912,558.112 468.912,599.524 468.912,599.524 C468.912,599.524 350.375,599.524 350.375,599.524 C350.375,599.524 350.375,558.112 350.375,558.112 C350.375,558.112 290.154,558.112 290.154,558.112 C290.154,558.112 290.154,329.869 290.154,329.869 C290.154,329.869 481.052,329.869 481.052,329.869 C481.052,329.869 481.052,558.112 481.052,558.112 C481.052,558.112 468.912,558.112 468.912,558.112 Z" id="flat-a2-4" data-url="<?php echo $ap_array['4']['url']; ?>" class="<?php echo $ap_array['4']['status']; ?> fill-rule="evenodd" />
                    <path d="M183.756,558.826 C183.756,558.826 183.756,599.762 183.756,599.762 C183.756,599.762 65.457,599.762 65.457,599.762 C65.457,599.762 65.457,558.826 65.457,558.826 C65.457,558.826 41.893,558.826 41.893,558.826 C41.893,558.826 41.893,329.869 41.893,329.869 C41.893,329.869 288.250,329.869 288.250,329.869 C288.250,329.869 288.250,558.826 288.250,558.826 C288.250,558.826 183.756,558.826 183.756,558.826 Z" id="flat-a2-5" data-url="<?php echo $ap_array['5']['url']; ?>" class="<?php echo $ap_array['5']['status']; ?> fill-rule="evenodd" />
                    <path d="M243.263,270.369 C243.263,270.369 243.263,333.915 243.263,333.915 C243.263,333.915 139.484,333.915 139.484,333.915 C139.484,333.915 139.484,339.389 139.484,339.389 C139.484,339.389 41.893,339.389 41.893,339.389 C41.893,339.389 41.893,290.837 41.893,290.837 C41.893,290.837 0.000,290.837 0.000,290.837 C0.000,290.837 0.000,126.616 0.000,126.616 C0.000,126.616 41.893,126.616 41.893,126.616 C41.893,126.616 41.893,41.888 41.893,41.888 C41.893,41.888 139.484,41.888 139.484,41.888 C139.484,41.888 139.484,194.209 139.484,194.209 C139.484,194.209 270.636,194.209 270.636,194.209 C270.636,194.209 270.636,270.369 270.636,270.369 C270.636,270.369 243.263,270.369 243.263,270.369 Z" id="flat-a2-6" data-url="<?php echo $ap_array['6']['url']; ?>" class="<?php echo $ap_array['6']['status']; ?> fill-rule="evenodd" />
                    <path d="M350.137,197.303 C350.137,197.303 350.137,267.037 350.137,267.037 C350.137,267.037 266.352,267.037 266.352,267.037 C266.352,267.037 266.352,197.303 266.352,197.303 C266.352,197.303 138.055,197.303 138.055,197.303 C138.055,197.303 138.055,41.888 138.055,41.888 C138.055,41.888 194.706,41.888 194.706,41.888 C194.706,41.888 194.706,0.000 194.706,0.000 C194.706,0.000 330.143,0.000 330.143,0.000 C330.143,0.000 330.143,41.888 330.143,41.888 C330.143,41.888 354.183,41.888 354.183,41.888 C354.183,41.888 354.183,197.303 354.183,197.303 C354.183,197.303 350.137,197.303 350.137,197.303 Z" id="flat-a2-7" data-url="<?php echo $ap_array['7']['url']; ?>" class="<?php echo $ap_array['7']['status']; ?> fill-rule="evenodd" />
                  </svg>
                 <?php
              foreach ($ap_array as $value) { ?>
    							
    					<div class="flat-mark" id="mark-a2-<?php echo $value['pos'] ?>" data-url="<?php echo $value['url'] ?>">                  
                    <div class="floor-circle">
                      <div class="circle">
                        <div class="circle-in"><?php echo $value['id'] ?></div>
                      </div>
                      <div class="floor-pop-snap">
                        <div class="floor-pop free">
                          <p><?php _e('Kaina: ', 'theme_site_domainname'); ?><?php echo number_format($value['price'], 0, ',', ' '); ?> €</p>
                          <p><?php _e('Plotas: ', 'theme_site_domainname'); ?><?php echo $value['area'] ?> m<sup>2</sup></p>
                          <p><?php _e('Kambarių skaičus: ', 'theme_site_domainname'); ?><?php echo $value['rooms'] ?></p>
                        </div>
                      </div>
                    </div>                  
                  </div>
				<?php }
				
			  ?></div>
			  <?php

						        break;
								case 3:?>
								<div class="floor-plan" id="floor-a3">
                                   
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="825" height="600" viewBox="0 0 825 600">
                    <defs>
                      <pattern id="free" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a3.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="hover" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a3-hover.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="reserved" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a3-reserved.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="sold" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a3-sold.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                    </defs>
                    <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a3.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                    <path d="M783.107,196.589 C783.107,196.589 565.789,196.589 565.789,196.589 C565.789,196.589 565.789,264.895 565.789,264.895 C565.789,264.895 490.335,264.895 490.335,264.895 C490.335,264.895 490.335,196.589 490.335,196.589 C490.335,196.589 476.529,196.589 476.529,196.589 C476.529,196.589 476.529,41.650 476.529,41.650 C476.529,41.650 531.751,41.650 531.751,41.650 C531.751,41.650 531.751,0.000 531.751,0.000 C531.751,0.000 650.288,0.000 650.288,0.000 C650.288,0.000 650.288,41.650 650.288,41.650 C650.288,41.650 783.107,41.650 783.107,41.650 C783.107,41.650 783.107,196.589 783.107,196.589 Z" id="flat-a3-1" data-url="<?php echo $ap_array['1']['url']; ?>" class="<?php echo $ap_array['1']['status']; ?> fill-rule="evenodd" />
                    <path d="M824.762,330.107 C824.762,330.107 783.107,330.107 783.107,330.107 C783.107,330.107 783.107,396.271 783.107,396.271 C783.107,396.271 564.361,396.271 564.361,396.271 C564.361,396.271 564.361,197.303 564.361,197.303 C564.361,197.303 783.107,197.303 783.107,197.303 C783.107,197.303 783.107,211.583 783.107,211.583 C783.107,211.583 824.762,211.583 824.762,211.583 C824.762,211.583 824.762,330.107 824.762,330.107 Z" id="flat-a3-2" data-url="<?php echo $ap_array['2']['url']; ?>" class="<?php echo $ap_array['2']['status']; ?> fill-rule="evenodd" />
                    <path d="M783.107,558.112 C783.107,558.112 639.101,558.112 639.101,558.112 C639.101,558.112 639.101,599.762 639.101,599.762 C639.101,599.762 526.515,599.762 526.515,599.762 C526.515,599.762 526.515,558.112 526.515,558.112 C526.515,558.112 479.147,558.112 479.147,558.112 C479.147,558.112 479.147,329.869 479.147,329.869 C479.147,329.869 565.551,329.869 565.551,329.869 C565.551,329.869 565.551,402.221 565.551,402.221 C565.551,402.221 783.107,402.221 783.107,402.221 C783.107,402.221 783.107,558.112 783.107,558.112 Z" id="flat-a3-3" data-url="<?php echo $ap_array['3']['url']; ?>" class="<?php echo $ap_array['3']['status']; ?> fill-rule="evenodd" />
                    <path d="M468.912,558.112 C468.912,558.112 468.912,599.524 468.912,599.524 C468.912,599.524 350.375,599.524 350.375,599.524 C350.375,599.524 350.375,558.112 350.375,558.112 C350.375,558.112 290.154,558.112 290.154,558.112 C290.154,558.112 290.154,329.869 290.154,329.869 C290.154,329.869 481.052,329.869 481.052,329.869 C481.052,329.869 481.052,558.112 481.052,558.112 C481.052,558.112 468.912,558.112 468.912,558.112 Z" id="flat-a3-4" data-url="<?php echo $ap_array['4']['url']; ?>" class="<?php echo $ap_array['4']['status']; ?> fill-rule="evenodd" />
                    <path d="M183.756,558.826 C183.756,558.826 183.756,599.762 183.756,599.762 C183.756,599.762 65.457,599.762 65.457,599.762 C65.457,599.762 65.457,558.826 65.457,558.826 C65.457,558.826 41.893,558.826 41.893,558.826 C41.893,558.826 41.893,329.869 41.893,329.869 C41.893,329.869 288.250,329.869 288.250,329.869 C288.250,329.869 288.250,558.826 288.250,558.826 C288.250,558.826 183.756,558.826 183.756,558.826 Z" id="flat-a3-5" data-url="<?php echo $ap_array['5']['url']; ?>" class="<?php echo $ap_array['5']['status']; ?> fill-rule="evenodd" />
                    <path d="M243.263,270.369 C243.263,270.369 243.263,333.915 243.263,333.915 C243.263,333.915 139.484,333.915 139.484,333.915 C139.484,333.915 139.484,339.389 139.484,339.389 C139.484,339.389 41.893,339.389 41.893,339.389 C41.893,339.389 41.893,290.837 41.893,290.837 C41.893,290.837 0.000,290.837 0.000,290.837 C0.000,290.837 0.000,126.616 0.000,126.616 C0.000,126.616 41.893,126.616 41.893,126.616 C41.893,126.616 41.893,41.888 41.893,41.888 C41.893,41.888 139.484,41.888 139.484,41.888 C139.484,41.888 139.484,194.209 139.484,194.209 C139.484,194.209 270.636,194.209 270.636,194.209 C270.636,194.209 270.636,270.369 270.636,270.369 C270.636,270.369 243.263,270.369 243.263,270.369 Z" id="flat-a3-6" data-url="<?php echo $ap_array['6']['url']; ?>" class="<?php echo $ap_array['6']['status']; ?> fill-rule="evenodd" />
                    <path d="M350.137,197.303 C350.137,197.303 350.137,267.037 350.137,267.037 C350.137,267.037 266.352,267.037 266.352,267.037 C266.352,267.037 266.352,197.303 266.352,197.303 C266.352,197.303 138.055,197.303 138.055,197.303 C138.055,197.303 138.055,41.888 138.055,41.888 C138.055,41.888 194.706,41.888 194.706,41.888 C194.706,41.888 194.706,0.000 194.706,0.000 C194.706,0.000 330.143,0.000 330.143,0.000 C330.143,0.000 330.143,41.888 330.143,41.888 C330.143,41.888 354.183,41.888 354.183,41.888 C354.183,41.888 354.183,197.303 354.183,197.303 C354.183,197.303 350.137,197.303 350.137,197.303 Z" id="flat-a3-7" data-url="<?php echo $ap_array['7']['url']; ?>" class="<?php echo $ap_array['7']['status']; ?> fill-rule="evenodd" />
                  </svg>
                  
                   <?php
              foreach ($ap_array as $value) { ?>
    							
    					<div class="flat-mark" id="mark-a3-<?php echo $value['pos'] ?>" data-url="<?php echo $value['url'] ?>">                  
                    <div class="floor-circle">
                      <div class="circle">
                        <div class="circle-in"><?php echo $value['id'] ?></div>
                      </div>
                      <div class="floor-pop-snap">
                        <div class="floor-pop free">
                          <p><?php _e('Kaina: ', 'theme_site_domainname'); ?><?php echo number_format($value['price'], 0, ',', ' '); ?> €</p>
                          <p><?php _e('Plotas: ', 'theme_site_domainname'); ?><?php echo $value['area'] ?> m<sup>2</sup></p>
                          <p><?php _e('Kambarių skaičus: ', 'theme_site_domainname'); ?><?php echo $value['rooms'] ?></p>
                        </div>
                      </div>
                    </div>                  
                  </div>
				<?php }
				
			  ?></div>
			  <?php
								
						        break;
								case 4:
						        
								?>
								<div class="floor-plan" id="floor-a4">
                              
                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="825" height="600" viewBox="0 0 825 600">
                    <defs>
                      <pattern id="free" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a4.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="hover" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a4-hover.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="reserved" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a4-reserved.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="sold" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a4-sold.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                    </defs>
                    <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a4.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />

      <path d="M783.107,196.589 C783.107,196.589 565.789,196.589 565.789,196.589 C565.789,196.589 565.789,264.895 565.789,264.895 C565.789,264.895 490.335,264.895 490.335,264.895 C490.335,264.895 490.335,196.589 490.335,196.589 C490.335,196.589 476.529,196.589 476.529,196.589 C476.529,196.589 476.529,41.650 476.529,41.650 C476.529,41.650 531.751,41.650 531.751,41.650 C531.751,41.650 531.751,0.000 531.751,0.000 C531.751,0.000 650.288,0.000 650.288,0.000 C650.288,0.000 650.288,41.650 650.288,41.650 C650.288,41.650 783.107,41.650 783.107,41.650 C783.107,41.650 783.107,196.589 783.107,196.589 Z" id="flat-a4-1" data-url="<?php echo $ap_array['1']['url']; ?>" class="<?php echo $ap_array['1']['status']; ?> fill-rule="evenodd"/>
      <path d="M824.762,330.107 C824.762,330.107 783.107,330.107 783.107,330.107 C783.107,330.107 783.107,396.271 783.107,396.271 C783.107,396.271 564.361,396.271 564.361,396.271 C564.361,396.271 564.361,197.303 564.361,197.303 C564.361,197.303 783.107,197.303 783.107,197.303 C783.107,197.303 783.107,211.583 783.107,211.583 C783.107,211.583 824.762,211.583 824.762,211.583 C824.762,211.583 824.762,330.107 824.762,330.107 Z" id="flat-a4-2" data-url="<?php echo $ap_array['2']['url']; ?>" class="<?php echo $ap_array['2']['status']; ?> fill-rule="evenodd"/>
      <path d="M783.107,558.112 C783.107,558.112 639.101,558.112 639.101,558.112 C639.101,558.112 639.101,599.762 639.101,599.762 C639.101,599.762 526.515,599.762 526.515,599.762 C526.515,599.762 526.515,558.112 526.515,558.112 C526.515,558.112 479.147,558.112 479.147,558.112 C479.147,558.112 479.147,329.869 479.147,329.869 C479.147,329.869 565.551,329.869 565.551,329.869 C565.551,329.869 565.551,402.221 565.551,402.221 C565.551,402.221 783.107,402.221 783.107,402.221 C783.107,402.221 783.107,558.112 783.107,558.112 Z" id="flat-a4-3" data-url="<?php echo $ap_array['3']['url']; ?>" class="<?php echo $ap_array['3']['status']; ?> fill-rule="evenodd"/>
      <path d="M468.912,558.112 C468.912,558.112 468.912,599.524 468.912,599.524 C468.912,599.524 350.375,599.524 350.375,599.524 C350.375,599.524 350.375,558.112 350.375,558.112 C350.375,558.112 290.154,558.112 290.154,558.112 C290.154,558.112 290.154,329.869 290.154,329.869 C290.154,329.869 481.052,329.869 481.052,329.869 C481.052,329.869 481.052,558.112 481.052,558.112 C481.052,558.112 468.912,558.112 468.912,558.112 Z" id="flat-a4-4" data-url="<?php echo $ap_array['4']['url']; ?>" class="<?php echo $ap_array['4']['status']; ?> fill-rule="evenodd"/>
      <path d="M183.756,558.826 C183.756,558.826 183.756,599.762 183.756,599.762 C183.756,599.762 65.457,599.762 65.457,599.762 C65.457,599.762 65.457,558.826 65.457,558.826 C65.457,558.826 41.893,558.826 41.893,558.826 C41.893,558.826 41.893,329.869 41.893,329.869 C41.893,329.869 288.250,329.869 288.250,329.869 C288.250,329.869 288.250,558.826 288.250,558.826 C288.250,558.826 183.756,558.826 183.756,558.826 Z" id="flat-a4-5" data-url="<?php echo $ap_array['5']['url']; ?>" class="<?php echo $ap_array['5']['status']; ?> fill-rule="evenodd"/>
      <path d="M243.263,270.369 C243.263,270.369 243.263,333.915 243.263,333.915 C243.263,333.915 139.484,333.915 139.484,333.915 C139.484,333.915 139.484,339.389 139.484,339.389 C139.484,339.389 41.893,339.389 41.893,339.389 C41.893,339.389 41.893,290.837 41.893,290.837 C41.893,290.837 0.000,290.837 0.000,290.837 C0.000,290.837 0.000,171.837 0.000,171.837 C0.000,171.837 41.893,171.837 41.893,171.837 C41.893,171.837 41.893,41.888 41.893,41.888 C41.893,41.888 139.484,41.888 139.484,41.888 C139.484,41.888 139.484,194.209 139.484,194.209 C139.484,194.209 270.636,194.209 270.636,194.209 C270.636,194.209 270.636,270.369 270.636,270.369 C270.636,270.369 243.263,270.369 243.263,270.369 Z" id="flat-a4-6" data-url="<?php echo $ap_array['6']['url']; ?>" class="<?php echo $ap_array['6']['status']; ?> fill-rule="evenodd"/>
      <path d="M350.137,197.303 C350.137,197.303 350.137,267.037 350.137,267.037 C350.137,267.037 266.352,267.037 266.352,267.037 C266.352,267.037 266.352,197.303 266.352,197.303 C266.352,197.303 138.055,197.303 138.055,197.303 C138.055,197.303 138.055,41.888 138.055,41.888 C138.055,41.888 194.706,41.888 194.706,41.888 C194.706,41.888 194.706,0.000 194.706,0.000 C194.706,0.000 330.143,0.000 330.143,0.000 C330.143,0.000 330.143,41.888 330.143,41.888 C330.143,41.888 354.183,41.888 354.183,41.888 C354.183,41.888 354.183,197.303 354.183,197.303 C354.183,197.303 350.137,197.303 350.137,197.303 Z" id="flat-a4-7" data-url="<?php echo $ap_array['7']['url']; ?>" class="<?php echo $ap_array['7']['status']; ?> fill-rule="evenodd"/>
     </svg>
                   <?php
              foreach ($ap_array as $value) { ?>
    							
    					<div class="flat-mark" id="mark-a4-<?php echo $value['pos'] ?>" data-url="<?php echo $value['url'] ?>">                  
                    <div class="floor-circle">
                      <div class="circle">
                        <div class="circle-in"><?php echo $value['id'] ?></div>
                      </div>
                      <div class="floor-pop-snap">
                        <div class="floor-pop free">
                          <p><?php _e('Kaina: ', 'theme_site_domainname'); ?><?php echo number_format($value['price'], 0, ',', ' '); ?> €</p>
                          <p><?php _e('Plotas: ', 'theme_site_domainname'); ?><?php echo $value['area'] ?> m<sup>2</sup></p>
                          <p><?php _e('Kambarių skaičus: ', 'theme_site_domainname'); ?><?php echo $value['rooms'] ?></p>
                        </div>
                      </div>
                    </div>                  
                  </div>
				<?php }
				
			  ?></div>
			  <?php
								
						        break;
								case 5:
						        
								?>
								<div class="floor-plan" id="floor-a5">
                 
                  
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="825" height="600" viewBox="0 0 825 600">
                    <defs>
                      <pattern id="free" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a5.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="hover" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a5-hover.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="reserved" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a5-reserved.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="sold" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a5-sold.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                    </defs>
                    <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a5.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />

                    
                    
                    <path d="M783.107,196.589 C783.107,196.589 625.296,196.589 625.296,196.589 C625.296,196.589 625.296,264.895 625.296,264.895 C625.296,264.895 490.335,264.895 490.335,264.895 C490.335,264.895 490.335,196.589 490.335,196.589 C490.335,196.589 476.529,196.589 476.529,196.589 C476.529,196.589 476.529,41.650 476.529,41.650 C476.529,41.650 531.751,41.650 531.751,41.650 C531.751,41.650 531.751,0.000 531.751,0.000 C531.751,0.000 650.288,0.000 650.288,0.000 C650.288,0.000 650.288,41.650 650.288,41.650 C650.288,41.650 783.107,41.650 783.107,41.650 C783.107,41.650 783.107,196.589 783.107,196.589 Z" id="flat-a5-1" data-url="<?php echo $ap_array['1']['url']; ?>" class="<?php echo $ap_array['1']['status']; ?> fill-rule="evenodd" />
                    <path d="M824.762,478.620 C824.762,478.620 782.869,478.620 782.869,478.620 C782.869,478.620 782.869,558.112 782.869,558.112 C782.869,558.112 627.914,558.112 627.914,558.112 C627.914,558.112 627.914,403.411 627.914,403.411 C627.914,403.411 564.361,403.411 564.361,403.411 C564.361,403.411 564.361,197.303 564.361,197.303 C564.361,197.303 783.107,197.303 783.107,197.303 C783.107,197.303 783.107,360.095 783.107,360.095 C783.107,360.095 824.762,360.095 824.762,360.095 C824.762,360.095 824.762,478.620 824.762,478.620 Z" id="flat-a5-2" data-url="<?php echo $ap_array['2']['url']; ?>" class="<?php echo $ap_array['2']['status']; ?> fill-rule="evenodd" />
                    <path d="M626.010,558.112 C626.010,558.112 579.595,558.112 579.595,558.112 C579.595,558.112 579.595,599.762 579.595,599.762 C579.595,599.762 471.769,599.762 471.769,599.762 C471.769,599.762 471.769,558.112 471.769,558.112 C471.769,558.112 412.500,558.112 412.500,558.112 C412.500,558.112 412.500,329.869 412.500,329.869 C412.500,329.869 565.551,329.869 565.551,329.869 C565.551,329.869 565.551,402.221 565.551,402.221 C565.551,402.221 626.010,402.221 626.010,402.221 C626.010,402.221 626.010,558.112 626.010,558.112 Z" id="flat-a5-3" data-url="<?php echo $ap_array['3']['url']; ?>" class="<?php echo $ap_array['3']['status']; ?> fill-rule="evenodd" />
                    <path d="M352.755,558.112 C352.755,558.112 352.755,599.524 352.755,599.524 C352.755,599.524 245.405,599.524 245.405,599.524 C245.405,599.524 245.405,558.112 245.405,558.112 C245.405,558.112 201.370,558.112 201.370,558.112 C201.370,558.112 201.370,399.603 201.370,399.603 C201.370,399.603 254.450,399.603 254.450,399.603 C254.450,399.603 254.450,329.869 254.450,329.869 C254.450,329.869 414.404,329.869 414.404,329.869 C414.404,329.869 414.404,558.112 414.404,558.112 C414.404,558.112 352.755,558.112 352.755,558.112 Z" id="flat-a5-4" data-url="<?php echo $ap_array['4']['url']; ?>" class="<?php echo $ap_array['4']['status']; ?> fill-rule="evenodd" />
                    <path d="M261.115,314.875 C261.115,314.875 257.307,314.875 257.307,314.875 C257.307,314.875 257.307,405.791 257.307,405.791 C257.307,405.791 197.034,405.791 197.034,405.791 C197.034,405.791 197.034,558.826 197.034,558.826 C197.034,558.826 41.893,558.826 41.893,558.826 C41.893,558.826 41.893,481.238 41.893,481.238 C41.893,481.238 -0.000,481.238 -0.000,481.238 C-0.000,481.238 -0.000,362.951 -0.000,362.951 C-0.000,362.951 41.893,362.951 41.893,362.951 C41.893,362.951 41.893,329.869 41.893,329.869 C41.893,329.869 41.893,199.445 41.893,199.445 C41.893,199.445 197.800,199.445 197.800,199.445 C197.800,199.445 197.800,269.179 197.800,269.179 C197.800,269.179 261.115,269.179 261.115,269.179 C261.115,269.179 261.115,314.875 261.115,314.875 Z" id="flat-a5-5" data-url="<?php echo $ap_array['5']['url']; ?>" class="<?php echo $ap_array['5']['status']; ?> fill-rule="evenodd" />
                    <path d="M354.183,203.491 C354.183,203.491 351.565,203.491 351.565,203.491 C351.565,203.491 351.565,267.989 351.565,267.989 C351.565,267.989 198.038,267.989 198.038,267.989 C198.038,267.989 198.038,203.491 198.038,203.491 C198.038,203.491 41.893,203.491 41.893,203.491 C41.893,203.491 41.893,41.888 41.893,41.888 C41.893,41.888 194.468,41.888 194.468,41.888 C194.468,41.888 194.468,0.238 194.468,0.238 C194.468,0.238 329.905,0.238 329.905,0.238 C329.905,0.238 329.905,41.888 329.905,41.888 C329.905,41.888 354.183,41.888 354.183,41.888 C354.183,41.888 354.183,203.491 354.183,203.491 Z" id="flat-a5-6" data-url="<?php echo $ap_array['6']['url']; ?>" class="<?php echo $ap_array['6']['status']; ?> fill-rule="evenodd" />
                  </svg>
                   <?php
              foreach ($ap_array as $value) { ?>
    							
    					<div class="flat-mark" id="mark-a5-<?php echo $value['pos'] ?>" data-url="<?php echo $value['url'] ?>">                  
                    <div class="floor-circle">
                      <div class="circle">
                        <div class="circle-in"><?php echo $value['id'] ?></div>
                      </div>
                      <div class="floor-pop-snap">
                        <div class="floor-pop free">
                          <p><?php _e('Kaina: ', 'theme_site_domainname'); ?><?php echo number_format($value['price'], 0, ',', ' '); ?> €</p>
                          <p><?php _e('Plotas: ', 'theme_site_domainname'); ?><?php echo $value['area'] ?> m<sup>2</sup></p>
                          <p><?php _e('Kambarių skaičus: ', 'theme_site_domainname'); ?><?php echo $value['rooms'] ?></p>
                        </div>
                      </div>
                    </div>                  
                  </div>
				<?php }
				
			  ?></div>
			  <?php
								
						        break;
								case 6:
						       
							   ?>
								<div class="floor-plan" id="floor-a6">
                                   
                  
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="825" height="600" viewBox="0 0 825 600">
                    <defs>
                      <pattern id="free" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a6.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="hover" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a6-hover.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="reserved" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a6-reserved.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="sold" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a6-sold.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                    </defs>
                    <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a6.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />


                    <path d="M783.107,196.589 C783.107,196.589 625.296,196.589 625.296,196.589 C625.296,196.589 625.296,264.895 625.296,264.895 C625.296,264.895 490.335,264.895 490.335,264.895 C490.335,264.895 490.335,196.589 490.335,196.589 C490.335,196.589 476.529,196.589 476.529,196.589 C476.529,196.589 476.529,41.650 476.529,41.650 C476.529,41.650 531.751,41.650 531.751,41.650 C531.751,41.650 531.751,0.000 531.751,0.000 C531.751,0.000 650.288,0.000 650.288,0.000 C650.288,0.000 650.288,41.650 650.288,41.650 C650.288,41.650 783.107,41.650 783.107,41.650 C783.107,41.650 783.107,196.589 783.107,196.589 Z" id="flat-a6-1" data-url="<?php echo $ap_array['1']['url']; ?>" class="<?php echo $ap_array['1']['status']; ?> fill-rule="evenodd" />
                    <path d="M824.762,478.620 C824.762,478.620 782.869,478.620 782.869,478.620 C782.869,478.620 782.869,558.112 782.869,558.112 C782.869,558.112 627.914,558.112 627.914,558.112 C627.914,558.112 627.914,403.411 627.914,403.411 C627.914,403.411 564.361,403.411 564.361,403.411 C564.361,403.411 564.361,197.303 564.361,197.303 C564.361,197.303 783.107,197.303 783.107,197.303 C783.107,197.303 783.107,360.095 783.107,360.095 C783.107,360.095 824.762,360.095 824.762,360.095 C824.762,360.095 824.762,478.620 824.762,478.620 Z" id="flat-a6-2" data-url="<?php echo $ap_array['2']['url']; ?>" class="<?php echo $ap_array['1']['status']; ?> fill-rule="evenodd" />
                    <path d="M626.010,558.112 C626.010,558.112 579.595,558.112 579.595,558.112 C579.595,558.112 579.595,599.762 579.595,599.762 C579.595,599.762 471.769,599.762 471.769,599.762 C471.769,599.762 471.769,558.112 471.769,558.112 C471.769,558.112 412.500,558.112 412.500,558.112 C412.500,558.112 412.500,329.869 412.500,329.869 C412.500,329.869 565.551,329.869 565.551,329.869 C565.551,329.869 565.551,402.221 565.551,402.221 C565.551,402.221 626.010,402.221 626.010,402.221 C626.010,402.221 626.010,558.112 626.010,558.112 Z" id="flat-a6-3" data-url="<?php echo $ap_array['3']['url']; ?>" class="<?php echo $ap_array['2']['status']; ?> fill-rule="evenodd" />
                    <path d="M352.755,558.112 C352.755,558.112 352.755,599.524 352.755,599.524 C352.755,599.524 245.405,599.524 245.405,599.524 C245.405,599.524 245.405,558.112 245.405,558.112 C245.405,558.112 201.370,558.112 201.370,558.112 C201.370,558.112 201.370,399.603 201.370,399.603 C201.370,399.603 254.450,399.603 254.450,399.603 C254.450,399.603 254.450,329.869 254.450,329.869 C254.450,329.869 414.404,329.869 414.404,329.869 C414.404,329.869 414.404,558.112 414.404,558.112 C414.404,558.112 352.755,558.112 352.755,558.112 Z" id="flat-a6-4" data-url="<?php echo $ap_array['4']['url']; ?>" class="<?php echo $ap_array['3']['status']; ?> fill-rule="evenodd" />
                    <path d="M261.115,314.875 C261.115,314.875 257.307,314.875 257.307,314.875 C257.307,314.875 257.307,405.791 257.307,405.791 C257.307,405.791 197.034,405.791 197.034,405.791 C197.034,405.791 197.034,558.826 197.034,558.826 C197.034,558.826 41.893,558.826 41.893,558.826 C41.893,558.826 41.893,481.238 41.893,481.238 C41.893,481.238 -0.000,481.238 -0.000,481.238 C-0.000,481.238 -0.000,362.951 -0.000,362.951 C-0.000,362.951 41.893,362.951 41.893,362.951 C41.893,362.951 41.893,329.869 41.893,329.869 C41.893,329.869 41.893,199.445 41.893,199.445 C41.893,199.445 197.800,199.445 197.800,199.445 C197.800,199.445 197.800,269.179 197.800,269.179 C197.800,269.179 261.115,269.179 261.115,269.179 C261.115,269.179 261.115,314.875 261.115,314.875 Z" id="flat-a6-5" data-url="<?php echo $ap_array['5']['url']; ?>" class="<?php echo $ap_array['5']['status']; ?> fill-rule="evenodd" />
                    <path d="M354.183,203.491 C354.183,203.491 351.565,203.491 351.565,203.491 C351.565,203.491 351.565,267.989 351.565,267.989 C351.565,267.989 198.038,267.989 198.038,267.989 C198.038,267.989 198.038,203.491 198.038,203.491 C198.038,203.491 41.893,203.491 41.893,203.491 C41.893,203.491 41.893,41.888 41.893,41.888 C41.893,41.888 194.468,41.888 194.468,41.888 C194.468,41.888 194.468,0.238 194.468,0.238 C194.468,0.238 329.905,0.238 329.905,0.238 C329.905,0.238 329.905,41.888 329.905,41.888 C329.905,41.888 354.183,41.888 354.183,41.888 C354.183,41.888 354.183,203.491 354.183,203.491 Z" id="flat-a6-6" data-url="<?php echo $ap_array['6']['url']; ?>" class="<?php echo $ap_array['6']['status']; ?> fill-rule="evenodd" />
                  </svg>
                   <?php
              foreach ($ap_array as $value) { ?>
    							
    					<div class="flat-mark" id="mark-a6-<?php echo $value['pos'] ?>" data-url="<?php echo $value['url'] ?>">                  
                    <div class="floor-circle">
                      <div class="circle">
                        <div class="circle-in"><?php echo $value['id'] ?></div>
                      </div>
                      <div class="floor-pop-snap">
                        <div class="floor-pop free">
                          <p><?php _e('Kaina: ', 'theme_site_domainname'); ?><?php echo number_format($value['price'], 0, ',', ' '); ?> €</p>
                          <p><?php _e('Plotas: ', 'theme_site_domainname'); ?><?php echo $value['area'] ?> m<sup>2</sup></p>
                          <p><?php _e('Kambarių skaičus: ', 'theme_site_domainname'); ?><?php echo $value['rooms'] ?></p>
                        </div>
                      </div>
                    </div>                  
                  </div>
				<?php }
				
			  ?></div>
			  <?php
							   
						        break;
								case 7:
						        
								?>
								<div class="floor-plan" id="floor-a7">
                  
                  
                  
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="825" height="600" viewBox="0 0 825 600">
                    <defs>
                      <pattern id="free" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a7.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="hover" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a7-hover.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="reserved" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a7-reserved.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="sold" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a7-sold.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                    </defs>
                    <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a7.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />

                    <path d="M783.107,196.589 C783.107,196.589 625.296,196.589 625.296,196.589 C625.296,196.589 625.296,264.895 625.296,264.895 C625.296,264.895 490.335,264.895 490.335,264.895 C490.335,264.895 490.335,196.589 490.335,196.589 C490.335,196.589 476.529,196.589 476.529,196.589 C476.529,196.589 476.529,41.650 476.529,41.650 C476.529,41.650 531.751,41.650 531.751,41.650 C531.751,41.650 531.751,0.000 531.751,0.000 C531.751,0.000 650.288,0.000 650.288,0.000 C650.288,0.000 650.288,41.650 650.288,41.650 C650.288,41.650 783.107,41.650 783.107,41.650 C783.107,41.650 783.107,196.589 783.107,196.589 Z" id="flat-a7-1" data-url="<?php echo $ap_array['1']['url']; ?>" class="<?php echo $ap_array['1']['status']; ?> fill-rule="evenodd" />
                    <path d="M824.762,478.620 C824.762,478.620 782.869,478.620 782.869,478.620 C782.869,478.620 782.869,558.112 782.869,558.112 C782.869,558.112 627.914,558.112 627.914,558.112 C627.914,558.112 627.914,403.411 627.914,403.411 C627.914,403.411 564.361,403.411 564.361,403.411 C564.361,403.411 564.361,197.303 564.361,197.303 C564.361,197.303 783.107,197.303 783.107,197.303 C783.107,197.303 783.107,360.095 783.107,360.095 C783.107,360.095 824.762,360.095 824.762,360.095 C824.762,360.095 824.762,478.620 824.762,478.620 Z" id="flat-a7-2" data-url="<?php echo $ap_array['2']['url']; ?>" class="<?php echo $ap_array['2']['status']; ?> fill-rule="evenodd" />
                    <path d="M626.010,558.112 C626.010,558.112 579.357,558.112 579.357,558.112 C579.357,558.112 579.357,599.762 579.357,599.762 C579.357,599.762 471.769,599.762 471.769,599.762 C471.769,599.762 471.769,558.112 471.769,558.112 C471.769,558.112 412.500,558.112 412.500,558.112 C412.500,558.112 412.500,329.869 412.500,329.869 C412.500,329.869 565.551,329.869 565.551,329.869 C565.551,329.869 565.551,402.221 565.551,402.221 C565.551,402.221 626.010,402.221 626.010,402.221 C626.010,402.221 626.010,558.112 626.010,558.112 Z" id="flat-a7-3" data-url="<?php echo $ap_array['3']['url']; ?>" class="<?php echo $ap_array['3']['status']; ?> fill-rule="evenodd" />
                    <path d="M352.755,558.112 C352.755,558.112 352.755,599.524 352.755,599.524 C352.755,599.524 245.405,599.524 245.405,599.524 C245.405,599.524 245.405,558.112 245.405,558.112 C245.405,558.112 201.370,558.112 201.370,558.112 C201.370,558.112 201.370,399.603 201.370,399.603 C201.370,399.603 254.450,399.603 254.450,399.603 C254.450,399.603 254.450,329.869 254.450,329.869 C254.450,329.869 414.404,329.869 414.404,329.869 C414.404,329.869 414.404,558.112 414.404,558.112 C414.404,558.112 352.755,558.112 352.755,558.112 Z" id="flat-a7-4" data-url="<?php echo $ap_array['4']['url']; ?>" class="<?php echo $ap_array['4']['status']; ?> fill-rule="evenodd" />
                    <path d="M261.115,314.875 C261.115,314.875 257.307,314.875 257.307,314.875 C257.307,314.875 257.307,405.791 257.307,405.791 C257.307,405.791 197.034,405.791 197.034,405.791 C197.034,405.791 197.034,558.826 197.034,558.826 C197.034,558.826 41.893,558.826 41.893,558.826 C41.893,558.826 41.893,481.238 41.893,481.238 C41.893,481.238 -0.000,481.238 -0.000,481.238 C-0.000,481.238 -0.000,362.951 -0.000,362.951 C-0.000,362.951 41.893,362.951 41.893,362.951 C41.893,362.951 41.893,329.869 41.893,329.869 C41.893,329.869 41.893,199.445 41.893,199.445 C41.893,199.445 197.800,199.445 197.800,199.445 C197.800,199.445 197.800,269.179 197.800,269.179 C197.800,269.179 261.115,269.179 261.115,269.179 C261.115,269.179 261.115,314.875 261.115,314.875 Z" id="flat-a7-5" data-url="<?php echo $ap_array['5']['url']; ?>" class="<?php echo $ap_array['5']['status']; ?> fill-rule="evenodd" />
                    <path d="M354.183,203.491 C354.183,203.491 351.565,203.491 351.565,203.491 C351.565,203.491 351.565,267.989 351.565,267.989 C351.565,267.989 198.038,267.989 198.038,267.989 C198.038,267.989 198.038,203.491 198.038,203.491 C198.038,203.491 41.893,203.491 41.893,203.491 C41.893,203.491 41.893,41.888 41.893,41.888 C41.893,41.888 194.468,41.888 194.468,41.888 C194.468,41.888 194.468,0.238 194.468,0.238 C194.468,0.238 329.905,0.238 329.905,0.238 C329.905,0.238 329.905,41.888 329.905,41.888 C329.905,41.888 354.183,41.888 354.183,41.888 C354.183,41.888 354.183,203.491 354.183,203.491 Z" id="flat-a7-6" data-url="<?php echo $ap_array['6']['url']; ?>" class="<?php echo $ap_array['6']['status']; ?> fill-rule="evenodd" />
                  </svg>
                   <?php
              foreach ($ap_array as $value) { ?>
    							
    					<div class="flat-mark" id="mark-a7-<?php echo $value['pos'] ?>" data-url="<?php echo $value['url'] ?>">                  
                    <div class="floor-circle">
                      <div class="circle">
                        <div class="circle-in"><?php echo $value['id'] ?></div>
                      </div>
                      <div class="floor-pop-snap">
                        <div class="floor-pop free">
                          <p><?php _e('Kaina: ', 'theme_site_domainname'); ?><?php echo number_format($value['price'], 0, ',', ' '); ?> €</p>
                          <p><?php _e('Plotas: ', 'theme_site_domainname'); ?><?php echo $value['area'] ?> m<sup>2</sup></p>
                          <p><?php _e('Kambarių skaičus: ', 'theme_site_domainname'); ?><?php echo $value['rooms'] ?></p>
                        </div>
                      </div>
                    </div>                  
                  </div>
				<?php }
				
			  ?></div>
			  <?php
								
						        break;
								case 8:
						        
								?>
								<div class="floor-plan" id="floor-a8">
                                    
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="825" height="600" viewBox="0 0 825 600">
                    <defs>
                      <pattern id="free" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a8.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="hover" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a8-hover.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="reserved" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a8-reserved.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="sold" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a8-sold.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                    </defs>
                    <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a8.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                    <path d="M782.857,264.895 C782.857,264.895 476.279,264.895 476.279,264.895 C476.279,264.895 476.279,41.888 476.279,41.888 C476.279,41.888 531.501,41.888 531.501,41.888 C531.501,41.888 531.501,0.000 531.501,0.000 C531.501,0.000 650.038,0.000 650.038,0.000 C650.038,0.000 650.038,41.888 650.038,41.888 C650.038,41.888 782.857,41.888 782.857,41.888 C782.857,41.888 782.857,264.895 782.857,264.895 Z" id="flat-a8-1" data-url="<?php echo $ap_array['1']['url']; ?>" class="<?php echo $ap_array['1']['status']; ?> fill-rule="evenodd" />
                    <path d="M824.750,436.493 C824.750,436.493 782.857,436.493 782.857,436.493 C782.857,436.493 782.857,558.112 782.857,558.112 C782.857,558.112 564.111,558.112 564.111,558.112 C564.111,558.112 564.111,262.277 564.111,262.277 C564.111,262.277 782.857,262.277 782.857,262.277 C782.857,262.277 782.857,317.969 782.857,317.969 C782.857,317.969 824.750,317.969 824.750,317.969 C824.750,317.969 824.750,436.493 824.750,436.493 Z" id="flat-a8-2" data-url="<?php echo $ap_array['2']['url']; ?>" class="<?php echo $ap_array['2']['status']; ?> fill-rule="evenodd" />
                    <path d="M521.980,558.112 C521.980,558.112 521.980,600.000 521.980,600.000 C521.980,600.000 414.154,600.000 414.154,600.000 C414.154,600.000 414.154,558.112 414.154,558.112 C414.154,558.112 412.250,558.112 412.250,558.112 C412.250,558.112 412.250,329.869 412.250,329.869 C412.250,329.869 565.539,329.869 565.539,329.869 C565.539,329.869 565.539,558.112 565.539,558.112 C565.539,558.112 521.980,558.112 521.980,558.112 Z" id="flat-a8-3" data-url="<?php echo $ap_array['3']['url']; ?>" class="<?php echo $ap_array['3']['status']; ?> fill-rule="evenodd" />
                    <path d="M352.981,558.112 C352.981,558.112 352.981,599.762 352.981,599.762 C352.981,599.762 245.155,599.762 245.155,599.762 C245.155,599.762 245.155,557.874 245.155,557.874 C245.155,557.874 200.406,557.874 200.406,557.874 C200.406,557.874 200.406,402.697 200.406,402.697 C200.406,402.697 254.914,402.697 254.914,402.697 C254.914,402.697 254.914,329.869 254.914,329.869 C254.914,329.869 407.965,329.869 407.965,329.869 C407.965,329.869 407.965,558.112 407.965,558.112 C407.965,558.112 352.981,558.112 352.981,558.112 Z" id="flat-a8-4" data-url="<?php echo $ap_array['4']['url']; ?>" class="<?php echo $ap_array['4']['status']; ?> fill-rule="evenodd" />
                    <path d="M198.740,402.697 C198.740,402.697 198.740,558.112 198.740,558.112 C198.740,558.112 41.643,558.112 41.643,558.112 C41.643,558.112 41.643,404.125 41.643,404.125 C41.643,404.125 -0.012,404.125 -0.012,404.125 C-0.012,404.125 -0.012,277.985 -0.012,277.985 C-0.012,277.985 41.643,277.985 41.643,277.985 C41.643,277.985 41.643,266.085 41.643,266.085 C41.643,266.085 259.437,266.085 259.437,266.085 C259.437,266.085 259.437,402.697 259.437,402.697 C259.437,402.697 198.740,402.697 198.740,402.697 Z" id="flat-a8-5" data-url="<?php echo $ap_array['5']['url']; ?>" class="<?php echo $ap_array['5']['status']; ?> fill-rule="evenodd" />
                    <path d="M354.171,267.037 C354.171,267.037 41.643,267.037 41.643,267.037 C41.643,267.037 41.643,41.888 41.643,41.888 C41.643,41.888 194.456,41.888 194.456,41.888 C194.456,41.888 194.456,0.000 194.456,0.000 C194.456,0.000 329.893,0.000 329.893,0.000 C329.893,0.000 329.893,41.888 329.893,41.888 C329.893,41.888 354.171,41.888 354.171,41.888 C354.171,41.888 354.171,267.037 354.171,267.037 Z" id="flat-a8-6" data-url="<?php echo $ap_array['6']['url']; ?>" class="<?php echo $ap_array['6']['status']; ?> fill-rule="evenodd" />
                  </svg>
                   <?php
              foreach ($ap_array as $value) { ?>
    							
    					<div class="flat-mark" id="mark-a8-<?php echo $value['pos'] ?>" data-url="<?php echo $value['url'] ?>">                  
                    <div class="floor-circle">
                      <div class="circle">
                        <div class="circle-in"><?php echo $value['id'] ?></div>
                      </div>
                      <div class="floor-pop-snap">
                        <div class="floor-pop free">
                          <p><?php _e('Kaina: ', 'theme_site_domainname'); ?><?php echo number_format($value['price'], 0, ',', ' '); ?> €</p>
                          <p><?php _e('Plotas: ', 'theme_site_domainname'); ?><?php echo $value['area'] ?> m<sup>2</sup></p>
                          <p><?php _e('Kambarių skaičus: ', 'theme_site_domainname'); ?><?php echo $value['rooms'] ?></p>
                        </div>
                      </div>
                    </div>                  
                  </div>
				<?php }
				
			  ?></div>
			  <?php
								
						        break;
								case 9:
						        
								?>
								<div class="floor-plan" id="floor-a9">
                                  
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="825" height="600" viewBox="0 0 825 600">
                    <defs>
                      <pattern id="free" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a9.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="hover" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a9-hover.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="reserved" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a9-reserved.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                      <pattern id="sold" patternUnits="userSpaceOnUse" width="825" height="600" y="0" x="0">
                        <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a9-sold.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />
                      </pattern>
                    </defs>
                    <image width="825" xlink:href="<?php echo get_template_directory_uri(); ?>/img/floors/a9.jpg" height="600" y="0" x="0" preserveAspectRatio="none" />

                    <path d="M782.857,264.895 C782.857,264.895 476.279,264.895 476.279,264.895 C476.279,264.895 476.279,41.888 476.279,41.888 C476.279,41.888 531.501,41.888 531.501,41.888 C531.501,41.888 531.501,0.000 531.501,0.000 C531.501,0.000 650.038,0.000 650.038,0.000 C650.038,0.000 650.038,41.888 650.038,41.888 C650.038,41.888 782.857,41.888 782.857,41.888 C782.857,41.888 782.857,264.895 782.857,264.895 Z" id="flat-a9-1" data-url="<?php echo $ap_array['1']['url']; ?>" class="<?php echo $ap_array['1']['status']; ?> fill-rule="evenodd" />
                    <path d="M824.750,436.493 C824.750,436.493 782.857,436.493 782.857,436.493 C782.857,436.493 782.857,558.112 782.857,558.112 C782.857,558.112 564.111,558.112 564.111,558.112 C564.111,558.112 564.111,262.277 564.111,262.277 C564.111,262.277 782.857,262.277 782.857,262.277 C782.857,262.277 782.857,317.969 782.857,317.969 C782.857,317.969 824.750,317.969 824.750,317.969 C824.750,317.969 824.750,436.493 824.750,436.493 Z" id="flat-a9-2" data-url="<?php echo $ap_array['2']['url']; ?>" class="<?php echo $ap_array['2']['status']; ?> fill-rule="evenodd" />
                    <path d="M521.980,558.112 C521.980,558.112 521.980,600.000 521.980,600.000 C521.980,600.000 414.154,600.000 414.154,600.000 C414.154,600.000 414.154,558.112 414.154,558.112 C414.154,558.112 412.250,558.112 412.250,558.112 C412.250,558.112 412.250,329.869 412.250,329.869 C412.250,329.869 565.539,329.869 565.539,329.869 C565.539,329.869 565.539,558.112 565.539,558.112 C565.539,558.112 521.980,558.112 521.980,558.112 Z" id="flat-a9-3" data-url="<?php echo $ap_array['3']['url']; ?>" class="<?php echo $ap_array['3']['status']; ?> fill-rule="evenodd" />
                    <path d="M352.981,558.112 C352.981,558.112 352.981,599.762 352.981,599.762 C352.981,599.762 245.155,599.762 245.155,599.762 C245.155,599.762 245.155,557.874 245.155,557.874 C245.155,557.874 200.406,557.874 200.406,557.874 C200.406,557.874 200.406,402.697 200.406,402.697 C200.406,402.697 254.914,402.697 254.914,402.697 C254.914,402.697 254.914,329.869 254.914,329.869 C254.914,329.869 407.965,329.869 407.965,329.869 C407.965,329.869 407.965,558.112 407.965,558.112 C407.965,558.112 352.981,558.112 352.981,558.112 Z" id="flat-a9-4" data-url="<?php echo $ap_array['4']['url']; ?>" class="<?php echo $ap_array['4']['status']; ?> fill-rule="evenodd" />
                    <path d="M198.740,402.697 C198.740,402.697 198.740,558.112 198.740,558.112 C198.740,558.112 41.643,558.112 41.643,558.112 C41.643,558.112 41.643,402.697 41.643,402.697 C41.643,402.697 41.643,398.413 41.643,398.413 C41.643,398.413 41.643,396.271 41.643,396.271 C41.643,396.271 -0.012,396.271 -0.012,396.271 C-0.012,396.271 -0.012,277.985 -0.012,277.985 C-0.012,277.985 41.643,277.985 41.643,277.985 C41.643,277.985 41.643,266.085 41.643,266.085 C41.643,266.085 259.437,266.085 259.437,266.085 C259.437,266.085 259.437,402.697 259.437,402.697 C259.437,402.697 198.740,402.697 198.740,402.697 Z" id="flat-a9-5" data-url="<?php echo $ap_array['5']['url']; ?>" class="<?php echo $ap_array['5']['status']; ?> fill-rule="evenodd" />
                    <path d="M354.171,267.037 C354.171,267.037 41.643,267.037 41.643,267.037 C41.643,267.037 41.643,41.888 41.643,41.888 C41.643,41.888 194.456,41.888 194.456,41.888 C194.456,41.888 194.456,0.000 194.456,0.000 C194.456,0.000 329.893,0.000 329.893,0.000 C329.893,0.000 329.893,41.888 329.893,41.888 C329.893,41.888 354.171,41.888 354.171,41.888 C354.171,41.888 354.171,267.037 354.171,267.037 Z" id="flat-a9-6" data-url="<?php echo $ap_array['6']['url']; ?>" class="<?php echo $ap_array['6']['status']; ?> fill-rule="evenodd" />
                  </svg>
                   <?php
              foreach ($ap_array as $value) { ?>
    							
    					<div class="flat-mark" id="mark-a9-<?php echo $value['pos'] ?>" data-url="<?php echo $value['url'] ?>">                  
                    <div class="floor-circle">
                      <div class="circle">
                        <div class="circle-in"><?php echo $value['id'] ?></div>
                      </div>
                      <div class="floor-pop-snap">
                        <div class="floor-pop free">
                          <p><?php _e('Kaina: ', 'theme_site_domainname'); ?><?php echo number_format($value['price'], 0, ',', ' '); ?> €</p>
                          <p><?php _e('Plotas: ', 'theme_site_domainname'); ?><?php echo $value['area'] ?> m<sup>2</sup></p>
                          <p><?php _e('Kambarių skaičus: ', 'theme_site_domainname'); ?><?php echo $value['rooms'] ?></p>
                        </div>
                      </div>
                    </div>                  
                  </div>
				<?php }
				
			  ?></div>
			  <?php
								
						        break;
						        }
						?>
                	
                
              </div>
            </div>
          </div>
          <div id="slide-overlay">
            <div class="slide-side">
              <div class="close-side">                
<!--
                  <svg width="15.556" height="15.556" viewBox="0 0 15.556 15.557">
                    <g>
                      <path d="m2.417 1.002l14.142 14.143-1.414 1.414-14.142-14.143 1.414-1.414z"/>
                      <path d="m16.559 2.416l-14.142 14.143-1.414-1.414 14.142-14.143 1.414 1.414z"/>
                    </g>
                    </svg>                
-->
              </div>
              <?php 
              
              $post_object = get_field('related_contact');
				
				if( $post_object ): 

					$post = $post_object;
					setup_postdata( $post ); 
					
					
					?>
              <div class="contact-broker">
                <h3><?php _e('Susisiekite', 'theme_site_domainname'); ?></h3>
                <div class="broker">
                  <img src="<?php the_field('image'); ?>" alt="<?php the_field('name'); ?>" />
                  <h5 class="brokername"><?php the_field('name'); ?></h5>
                  <span class="brokerinfo">
<?php the_field('phone'); ?><br />
<?php the_field('email'); ?>           
            </span>
                </div>
                <form id="brokerform" name="brokerform">
                  <input type="text" id="brokername" name="brokername" placeholder="<?php _e('Vardas', 'theme_site_domainname'); ?>">
                  <input type="text" id="brokeremail" name="brokeremailemail" placeholder="<?php _e('Telefonas arba el. paštas', 'theme_site_domainname'); ?>">
                  <textarea id="brokermessage" name="brokermessagemessage" placeholder="<?php _e('Pranešimas', 'theme_site_domainname'); ?>"></textarea>
                  <input type="button" id="brokersubmit" class="button" value="<?php _e('Siųsti', 'theme_site_domainname'); ?>">          
                </form>
              </div>
            </div>
          </div>
        </div>
<?php 
		wp_reset_postdata();
		endif;
                	endwhile;
}
get_footer(); ?>
