<?php
/**
 * Template Name: Map
 */

get_header();
get_template_part( 'subheader' );
?>
         <div id="mob-map" class="section">
              <div id="map-canvas">
              </div>
        </div>
      </div>
    </div>
  </div>
<?php 

get_footer(); 
?>