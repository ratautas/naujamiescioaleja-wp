<?php
global $suffix;
global $lang;
global $langs;
$suffix = '';
$suffix = '-' . qtranxf_getLanguage();
$lang = strtoupper(qtranxf_getLanguage());
$langs = qtranxf_getLanguage();
?>
<?php 

wp_reset_query();

$args = array(  
'name' => 'config' . $suffix,
'post_type' => 'site_config'
);
$the_query = new WP_Query($args);
if ($the_query->have_posts()) { while ($the_query->have_posts()) {  $the_query->the_post();      

		/*
        global $breadcrumb_home_title;
        $breadcrumb_home_title = get_field('breadcrumb_home_title');
        global $breadcrumb_about_title;
        $breadcrumb_about_title = get_field('breadcrumb_about_title');
		global $breadcrumb_about_url;
        $breadcrumb_about_url = get_field('breadcrumb_about_url');
		global $breadcrumb_news_title;
        $breadcrumb_news_title = get_field('breadcrumb_news_title');
		global $breadcrumb_news_url;
        $breadcrumb_news_url = get_field('breadcrumb_news_url');
		global $breadcrumb_apartments_title;
        $breadcrumb_apartments_title = get_field('breadcrumb_apartments_title');
		global $breadcrumb_apartments_url;
        $breadcrumb_apartments_url = get_field('breadcrumb_apartments_url');
		global $config_term_more;
        $config_term_more = get_field('config_term_more');
		$config_fb_link = get_field('config_fb_link');
        $config_footer_text = get_field('config_footer_text');
		$panel_features = '';
		$nr = 0;
		$first = '';
        while (have_rows('config_panel_features')) {
            the_row();
			$nr++;
			$h = '<div class="plus"><div class="img" style="background-image: url(' . get_sub_field('config_panel_features_img') . ')"></div><div class="plus-data">' . get_sub_field('config_panel_features_txt') . '</div></div>';
			if ($nr == 1)
				$first = $h;
            $panel_features .= $h;
        }
		$panel_features .= $first;
		$firstbg = '';
		$bg = 0;
		while (have_rows('config_panel_backgrounds')) {
            the_row();
			$actbg =  $bg == 0 ? ' active' : '';
			$firstbg .= '<div class="bg' . $actbg . '" style="background-image: url(' .get_sub_field('config_panel_backgrounds_img') . ')"></div>';
			$bg++;
        }
		*/
	} 
}

wp_reset_query();
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html itemscope itemtype="http://schema.org/Other" class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html itemscope itemtype="http://schema.org/Other" class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html itemscope itemtype="http://schema.org/Other" <?php language_attributes(); ?>>
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# profile: http://ogp.me/ns/profile#">
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/fav.png">
<title><?php 
//$url = $_SERVER["REQUEST_URI"];
//$isItProd = strpos($url, 'products');

 if (is_front_page()) {
     bloginfo('description');
 //} else if ($isItProd) {
 //    echo 'Products | SITE';    
 } else {
     wp_title( '| Naujamiesčio Alėja', true, 'right' );
 }

?></title>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<script type="text/javascript">  
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";    
    var themeurl = "<?php echo get_stylesheet_directory_uri(); ?>";
    var root_url = "<?php echo get_stylesheet_directory_uri(); ?>";
</script>
<?php wp_head(); ?>
</head>
<body>






