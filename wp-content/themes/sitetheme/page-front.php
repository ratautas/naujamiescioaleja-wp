<?php
/**
 * Template Name: Front
 */

get_header();
get_template_part( 'subheader' );
?>

         

  <?php     if ( have_posts() ){ while ( have_posts() ) : the_post(); ?>
        <div id="landing" class="section">
          <div class="front">
            <div class="column">
              <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" class="front-logo" alt="logo">
              
               
   
                
              
              <div class="column-text">
                <p>
                	<?php the_content(); ?> 
                </p>
              </div>
              
              
              <a class="button flat-toggle" href="<?php the_field('house_1_link'); ?>"><?php _e('Rasti butą', 'theme_site_domainname'); ?></a>
            </div>
            <div class="right if-desk">
              <div class="front-selection">
                <div class="switch">
                  <div class="toggler">
                    <div class="toggler-icon"></div>
                  </div>
                  <span class="daytime"><?php _e('Diena', 'theme_site_domainname'); ?></span>
                </div>
                <div class="front-image">
                	<?php 
                  $h1l = get_field('house_1_location');
                  $h1a = get_field('house_1_link');                                 
                  $h1i = get_field('house_1_if');
                  $h2l = get_field('house_2_location');
                  $h2a = get_field('house_2_link');                  
                  $h2i = get_field('house_2_if');
                  $h3l = get_field('house_3_location');
                  $h3a = get_field('house_3_link');
                  $h3i = get_field('house_3_if');
                  
                  ?>
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="1920" height="1080" viewBox="0 0 1920 1080">
                    <g id="front-psdsvg">
                      <image xlink:href="<?php echo get_template_directory_uri(); ?>/img/front-night.jpg" y="0" x="0" id="day-img" preserveAspectRatio="xMidYMid" width="1920" height="1080" />
                      <image xlink:href="<?php echo get_template_directory_uri(); ?>/img/front-day.jpg" y="0" x="0" id="night-img" preserveAspectRatio="xMidYMid" width="1920" height="1080" />
                      <path data-url="<?php echo $h1a; ?>" id="build-path-a" d="M761.000,635.000 C761.000,635.000 760.000,628.000 760.000,628.000 C760.000,628.000 756.000,289.000 756.000,289.000 C756.000,289.000 974.000,276.000 974.000,276.000 C974.000,276.000 973.000,285.000 973.000,285.000 C973.000,285.000 1025.000,321.000 1025.000,321.000 C1025.000,321.000 1024.292,325.247 1024.068,326.595 C1024.025,326.849 1024.000,327.000 1024.000,327.000 C1024.000,327.000 1036.000,605.000 1036.000,605.000 C1036.000,605.000 1022.000,622.000 1022.000,622.000 C1022.000,622.000 1020.000,630.000 1020.000,630.000 C1020.000,630.000 761.000,635.000 761.000,635.000 Z" fill-rule="evenodd" />
                      <path id="build-path-b" d="M1148.000,614.000 C1148.000,614.000 1146.000,391.000 1146.000,391.000 C1146.000,391.000 1155.000,392.000 1155.000,392.000 C1155.000,392.000 1155.000,371.000 1155.000,371.000 C1155.000,371.000 1181.000,370.000 1181.000,370.000 C1181.000,370.000 1180.000,365.000 1180.000,365.000 C1180.000,365.000 1210.000,361.000 1210.000,361.000 C1210.000,361.000 1213.000,365.000 1213.000,365.000 C1213.000,365.000 1230.000,369.000 1230.000,369.000 C1230.000,369.000 1229.000,361.000 1229.000,361.000 C1229.000,361.000 1248.000,359.000 1248.000,359.000 C1248.000,359.000 1251.000,366.000 1251.000,366.000 C1251.000,366.000 1355.000,363.000 1355.000,363.000 C1355.000,363.000 1354.000,386.000 1354.000,386.000 C1354.000,386.000 1371.000,385.000 1371.000,385.000 C1371.000,385.000 1380.000,614.000 1380.000,614.000 C1380.000,614.000 1368.000,622.000 1368.000,622.000 C1368.000,622.000 1158.000,625.000 1158.000,625.000 C1158.000,625.000 1156.000,614.000 1156.000,614.000 C1156.000,614.000 1148.000,614.000 1148.000,614.000 Z" fill-rule="evenodd" />
                      <path id="build-path-c" d="M1535.000,241.000 C1535.000,241.000 1794.000,225.000 1794.000,225.000 C1794.000,225.000 1802.000,629.000 1802.000,629.000 C1802.000,629.000 1574.000,631.000 1565.000,630.000 C1556.000,629.000 1497.000,613.000 1497.000,613.000 C1497.000,613.000 1493.000,303.000 1493.000,303.000 C1493.000,303.000 1502.000,294.000 1502.000,294.000 C1502.000,294.000 1506.000,290.000 1506.000,290.000 C1506.000,290.000 1504.000,276.000 1504.000,276.000 C1504.000,276.000 1509.000,270.000 1509.000,270.000 C1509.000,270.000 1518.000,271.000 1518.000,271.000 C1518.000,271.000 1535.000,241.000 1535.000,241.000 Z" fill-rule="evenodd" />
                    </g>
                  </svg>
                  
                  <a href="<?php echo $h1a; ?>" class="build-button active <?php echo $h1i ? 'built' : 'not-built'; ?> build-a">
                    <div class="button-in"><span><?php echo $h1l; ?></span>
                      <div class="build-info">
                        <p>Buto skaičus: 36</p>
                        <p>Parduota: 4</p>
                        <p>Rezervuota: 11</p>
                      </div>
                    </div>
                  </a>
                  <a href="<?php echo $h2a; ?>" class="build-button <?php echo $h2i ? 'built' : 'not-built'; ?> build-b">
                    <div class="button-in"><span><span><?php echo $h2l; ?></span>
                      <div class="build-info">
                        <p>Namo statybos planuojamos baigti 2016 metais</p>
                      </div>
                    </div>
                  </a>
                  <a href="<?php echo $h3a; ?>" class="build-button <?php echo $h3i ? 'built' : 'not-built'; ?> build-c">
                    <div class="button-in"><span><?php echo $h3l; ?></span>
                      <div class="build-info">
                        <p>Namo statybos planuojamos baigti 2016 metais</p>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
 
  <?php endwhile;}?>  
 
<?php 

get_footer(); 
?>
